<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color">
    <div class="mobile-sidebar-header d-md-none">
        <div class="header-logo">
            <a href="{{ route('admin.dashboard') }}">Education</a>
        </div>
    </div>
    <div class="sidebar-menu-content">
        <ul class="nav nav-sidebar-menu sidebar-toggle-view">

            <li class="nav-item ">
                <a href="{{ route('admin.dashboard') }}" class="nav-link"><i
                    class="fa fa-home"></i><span>Dashboard</span>
                </a>
            </li>

            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>Category</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/add/category') style="display: block;"
                    @elseif(request()->path() == 'admin/add/subCategory') style="display: block;"
                    @elseif(request()->path() == 'admin/add/subSubCategory') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('add.category') }}" class="nav-link  @if(request()->path() == 'admin/add/category') bg-success @endif"><i class="fas fa-angle-right"></i>Add Category</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.subCategory') }}" class="nav-link  @if(request()->path() == 'admin/add/subCategory') bg-success @endif"><i class="fas fa-angle-right"></i>Add Sub Category</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.subSubCategory') }}" class="nav-link  @if(request()->path() == 'admin/add/subSubCategory') bg-success @endif"><i class="fas fa-angle-right"></i>Add Sub subCategory</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>Products</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/add/product') style="display: block;"
                    @elseif(request()->path() == 'admin/manage/product') style="display: block;"
                    @elseif(request()->path() == 'admin/add/product/color') style="display: block;"
                    @elseif(request()->path() == 'admin/featured/product') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('add.product') }}" class="nav-link  @if(request()->path() == 'admin/add/product') bg-success @endif"><i class="fas fa-angle-right"></i>Add Product</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('manage.product') }}" class="nav-link  @if(request()->path() == 'admin/manage/product') bg-success @endif"><i class="fas fa-angle-right"></i>Manage Product</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.product.color') }}" class="nav-link  @if(request()->path() == 'admin/add/product/color') bg-success @endif"><i class="fas fa-angle-right"></i>Add Product Color</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('featured.product') }}" class="nav-link  @if(request()->path() == 'admin/featured/product') bg-success @endif"><i class="fas fa-angle-right"></i>Featured Product</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>Home Page</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/add/slider') style="display: block;"
                    @elseif(request()->path() == 'admin/add/home/text') style="display: block;"
                    @elseif(request()->path() == 'admin/add/home/image') style="display: block;"
                    @elseif(request()->path() == 'admin/add/home/section') style="display: block;"
                    @elseif(request()->path() == 'admin/add/home/popup') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('add.slider') }}" class="nav-link  @if(request()->path() == 'admin/add/slider') bg-success @endif"><i class="fas fa-angle-right"></i>Add Slider</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.home.text') }}" class="nav-link  @if(request()->path() == 'admin/add/home/text') bg-success @endif"><i class="fas fa-angle-right"></i>Add Home Text</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.home.image') }}" class="nav-link  @if(request()->path() == 'admin/add/home/image') bg-success @endif"><i class="fas fa-angle-right"></i>Add Home Images</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.home.section') }}" class="nav-link  @if(request()->path() == 'admin/add/home/section') bg-success @endif"><i class="fas fa-angle-right"></i>Add Home Section</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.home.popup') }}" class="nav-link  @if(request()->path() == 'admin/add/home/popup') bg-success @endif"><i class="fas fa-angle-right"></i>Add Subscribe PopUp</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>Who We Serve</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/add/integrated') style="display: block;"
                    @elseif(request()->path() == 'admin/add/acute') style="display: block;"
                    @elseif(request()->path() == 'admin/add/surgery') style="display: block;"
                    @elseif(request()->path() == 'admin/add/long/term') style="display: block;"
                    @elseif(request()->path() == 'admin/add/education') style="display: block;"
                    @elseif(request()->path() == 'admin/add/home/health') style="display: block;"
                    @elseif(request()->path() == 'admin/add/physician') style="display: block;"
                    @elseif(request()->path() == 'admin/add/assisted') style="display: block;"
                    @elseif(request()->path() == 'admin/add/managed') style="display: block;"
                    @elseif(request()->path() == 'admin/add/independent') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('add.integrated') }}" class="nav-link  @if(request()->path() == 'admin/add/integrated') bg-success @endif"><i class="fas fa-angle-right"></i>Add Integrated</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.acute') }}" class="nav-link  @if(request()->path() == 'admin/add/acute') bg-success @endif"><i class="fas fa-angle-right"></i>Add Acute Care</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.surgery') }}" class="nav-link  @if(request()->path() == 'admin/add/surgery') bg-success @endif"><i class="fas fa-angle-right"></i>Add Surgery Centers</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.long.term') }}" class="nav-link  @if(request()->path() == 'admin/add/long/term') bg-success @endif"><i class="fas fa-angle-right"></i>Add Long-Term Care</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.education') }}" class="nav-link  @if(request()->path() == 'admin/add/education') bg-success @endif"><i class="fas fa-angle-right"></i>Add Education</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.home.health') }}" class="nav-link  @if(request()->path() == 'admin/add/home/health') bg-success @endif"><i class="fas fa-angle-right"></i>Add Home Health</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.physician') }}" class="nav-link  @if(request()->path() == 'admin/add/physician') bg-success @endif"><i class="fas fa-angle-right"></i>Add Physician Offices</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.assisted') }}" class="nav-link  @if(request()->path() == 'admin/add/assisted') bg-success @endif"><i class="fas fa-angle-right"></i>Add Assisted Living</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.managed') }}" class="nav-link  @if(request()->path() == 'admin/add/managed') bg-success @endif"><i class="fas fa-angle-right"></i>Add Managed Care</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.independent') }}" class="nav-link  @if(request()->path() == 'admin/add/independent') bg-success @endif"><i class="fas fa-angle-right"></i>Add Independent Service</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>Resources</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/add/tech/tips') style="display: block;"
                    @elseif(request()->path() == 'admin/add/blog/title') style="display: block;"
                    @elseif(request()->path() == 'admin/add/blog/category') style="display: block;"
                    @elseif(request()->path() == 'admin/add/blog') style="display: block;"
                    @elseif(request()->path() == 'admin/add/news/title') style="display: block;"
                    @elseif(request()->path() == 'admin/add/news/category') style="display: block;"
                    @elseif(request()->path() == 'admin/add/news') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('add.tech.tips') }}" class="nav-link  @if(request()->path() == 'admin/add/tech/tips') bg-success @endif"><i class="fas fa-angle-right"></i>Add Tech Tips</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.blog.title') }}" class="nav-link  @if(request()->path() == 'admin/add/blog/title') bg-success @endif"><i class="fas fa-angle-right"></i>Add Blog Title</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.blog.category') }}" class="nav-link  @if(request()->path() == 'admin/add/blog/category') bg-success @endif"><i class="fas fa-angle-right"></i>Add Blog Category</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.blog') }}" class="nav-link  @if(request()->path() == 'admin/add/blog') bg-success @endif"><i class="fas fa-angle-right"></i>Add Blog</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.news.title') }}" class="nav-link  @if(request()->path() == 'admin/add/news/title') bg-success @endif"><i class="fas fa-angle-right"></i>Add News Title</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.news.category') }}" class="nav-link  @if(request()->path() == 'admin/add/news/category') bg-success @endif"><i class="fas fa-angle-right"></i>Add News Category</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.news') }}" class="nav-link  @if(request()->path() == 'admin/add/news') bg-success @endif"><i class="fas fa-angle-right"></i>Add News</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>About Us</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/add/about/us') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('add.about.us') }}" class="nav-link  @if(request()->path() == 'admin/add/about/us') bg-success @endif"><i class="fas fa-angle-right"></i>Add About Us</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>Contact Us</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/add/contact') style="display: block;"
                    @elseif(request()->path() == 'admin/add/store/location') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('add.contact') }}" class="nav-link  @if(request()->path() == 'admin/add/contact') bg-success @endif"><i class="fas fa-angle-right"></i>Add Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('add.store.location') }}" class="nav-link  @if(request()->path() == 'admin/add/store/location') bg-success @endif"><i class="fas fa-angle-right"></i>Add Store Location</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i
                    class="fa fa-toolbox"></i><span>Subscribe Email</span>
                </a>
                <ul class="nav sub-group-menu"
                    @if(request()->path() == 'admin/show/subscribe/email') style="display: block;"
                    @endif>
                    <li class="nav-item">
                        <a href="{{ route('show.subscribe.email') }}" class="nav-link  @if(request()->path() == 'admin/show/subscribe/email') bg-success @endif"><i class="fas fa-angle-right"></i>Show Email</a>
                    </li>
                </ul>
            </li>


        </ul>
    </div>
</div>


