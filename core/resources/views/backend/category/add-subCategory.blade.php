@extends('backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="card-title mt-2">Add SubCategory</h4>
                    <p class="text-right">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addSubCategory">Add SubCategory</button>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table_id">
                                <thead class="">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($subCategories as $subCategory)
                                    <tr>
                                        <th scope="row">{{ $i++ }}</th>
                                        <td><b>{{ $subCategory->categoryName->name }}
                                        <td><b>{{ $subCategory->name }}
                                        <td><b>@if($subCategory->status == 1) <button class="btn btn-success">Active</button> @else <button class="btn btn-warning">Inactive</button>  @endif</td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="#editSubCategory-{{ $subCategory->id }}" data-toggle="modal" class="btn btn-primary btn-lg">Edit</a>
                                                <span class="ml-2"></span>
                                                <a href="#deleteSubCategory-{{ $subCategory->id }}" data-toggle="modal" class="btn btn-danger btn-lg">Delete</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <div id="deleteSubCategory-{{ $subCategory->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><i class="fa fa-trash text-danger" aria-hidden="true"></i></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('delete.subCategory') }}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <p>Are you want to delete this?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="id" value="{{ $subCategory->id }}">
                                                            <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="editSubCategory-{{ $subCategory->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edit SubCategory</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form name="editSubCategory-{{ $subCategory->id }}" action="{{ route('update.subCategory') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label class="control-label">Category<span class="text-danger">*</span></label>
                                                            <select name="category_id" class="form-control select2">
                                                                <option selected disabled>---Select Category---</option>
                                                                @foreach($categories as $category)
                                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Name<span class="text-danger">*</span></label>
                                                            <input type="text" name="name" value="{{ $subCategory->name }}" class="form-control" placeholder="name">
                                                            <input type="hidden" name="id" value="{{ $subCategory->id }}" class="form-control" placeholder="name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Status</label>
                                                            <select name="status" class="form-control">
                                                                @if($subCategory->status == 1)
                                                                    <option value="1" class="form-control">Active</option>
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                @else
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                    <option value="1" class="form-control">Active</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary btn-lg">Update Save</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        document.forms['editSubCategory-{{ $subCategory->id }}'].elements['category_id'].value = '{{ $subCategory->categoryName->id }}';
                                    </script>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addSubCategory" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add SubCategory</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('save.subCategory') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Category<span class="text-danger">*</span></label>
                            <select name="category_id" class="form-control select2">
                                <option selected disabled value="0">---Select Category---</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Name<span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" placeholder="Name">
                        </div>
                        <select name="status" class="form-control">
                            <option value="1" class="form-control">Active</option>
                            <option value="0" class="form-control">Inactive</option>
                        </select>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
