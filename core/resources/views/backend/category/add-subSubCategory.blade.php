@extends('backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="card-title mt-2">Add Sub SubCategory</h4>
                    <p class="text-right">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addSubCategory">Add Sub SubCategory</button>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table_id">
                                <thead class="">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Sub Category</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($subSubCategories as $subSubCategory)
                                    <tr>
                                        <th scope="row">{{ $i++ }}</th>
                                        <td><b>{{ $subSubCategory->subCategoryName->categoryName->name }}
                                        <td><b>{{ $subSubCategory->subCategoryName->name }}
                                        <td><b>{{ $subSubCategory->name }}
                                        <td><b>@if($subSubCategory->status == 1) <button class="btn btn-success">Active</button> @else <button class="btn btn-warning">Inactive</button>  @endif</td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="#editSubCategory-{{ $subSubCategory->id }}" data-toggle="modal" class="btn btn-primary btn-lg">Edit</a>
                                                <span class="ml-2"></span>
                                                <a href="#deleteSubCategory-{{ $subSubCategory->id }}" data-toggle="modal" class="btn btn-danger btn-lg">Delete</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <div id="deleteSubCategory-{{ $subSubCategory->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><i class="fa fa-trash text-danger" aria-hidden="true"></i></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('delete.subSubCategory') }}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <p>Are you want to delete this?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="id" value="{{ $subSubCategory->id }}">
                                                            <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="editSubCategory-{{ $subSubCategory->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edit SubCategory</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form name="editSubCategory-{{ $subSubCategory->id }}" action="{{ route('update.subSubCategory') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label class="control-label">Category<span class="text-danger">*</span></label>
                                                            <select name="category_id" id="c_id" onchange="gCategory();" class="form-control select2">
                                                                <option selected disabled>---Select Category---</option>
                                                                @foreach($categories as $category)
                                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Sub Category<span class="text-danger">*</span></label>
                                                            <select name="subcategory_id" class="form-control sc_id select2">
                                                                <option selected disabled>---Select Sub Category---</option>
                                                                @foreach($subCategories as $subCategory)
                                                                    <option value="{{ $subCategory->id }}">{{ $subCategory->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Name<span class="text-danger">*</span></label>
                                                            <input type="text" name="name" value="{{ $subSubCategory->name }}" class="form-control" placeholder="name">
                                                            <input type="hidden" name="id" value="{{ $subSubCategory->id }}" class="form-control" placeholder="name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Status</label>
                                                            <select name="status" class="form-control">
                                                                @if($subSubCategory->status == 1)
                                                                    <option value="1" class="form-control">Active</option>
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                @else
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                    <option value="1" class="form-control">Active</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary btn-lg">Update Save</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        document.forms['editSubCategory-{{ $subSubCategory->id }}'].elements['category_id'].value = '{{ $subSubCategory->subCategoryName->categoryName->id }}';
                                        document.forms['editSubCategory-{{ $subSubCategory->id }}'].elements['subcategory_id'].value = '{{ $subSubCategory->subCategoryName->id }}';
                                    </script>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addSubCategory" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Sub SubCategory</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('save.subSubCategory') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Category<span class="text-danger">*</span></label>
                            <select name="category_id" id="category_id" onchange="getCategory();" class="form-control select2">
                                <option selected disabled value="0">---Select Category---</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sub Category<span class="text-danger">*</span></label>
                            <select name="subcategory_id"  class="form-control subcategory_id select2">
                                <option selected disabled>---First select the Category---</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Name<span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" placeholder="Name">
                        </div>
                        <select name="status" class="form-control">
                            <option value="1" class="form-control">Active</option>
                            <option value="0" class="form-control">Inactive</option>
                        </select>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

    </script>
@endsection
