@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Subscribe PopUp</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Subscribe PopUp</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form action="{{ route('update.home.popup') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($popup['image']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$popup['image']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image</label>
                        <input type="file" name="image" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Main Title <span class="text-danger">*</span></label>
                        <input type="text" name="title1" value="{{ $popup['title1'] }}" placeholder="" class="form-control">
                        <input type="hidden" name="id" value="{{ $popup['id'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title <span class="text-danger">*</span></label>
                        <input type="text" name="title2" value="{{ $popup['title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Placeholder Text <span class="text-danger">*</span></label>
                        <input type="text" name="place" value="{{ $popup['place'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Button <span class="text-danger">*</span></label>
                        <input type="text" name="button" value="{{ $popup['button'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Description <span class="text-danger">*</span></label>
                        <input type="text" name="des" value="{{ $popup['des'] }}" placeholder="" class="form-control">
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
@endsection
