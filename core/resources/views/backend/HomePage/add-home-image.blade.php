@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Home Page Images</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Home Page Images</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form action="{{ route('update.home.image') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image1']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image1']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Fevicon Icon</label>
                        <input type="file" name="image1" class="form-control-file" accept="image/*">
                        <input type="hidden" name="id" value="{{ $home->id }}" class="form-control-file">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image2']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image2']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Logo</label>
                        <input type="file" name="image2" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image3']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image3']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image1</label>
                        <input type="file" name="image3" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image4']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image4']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image2</label>
                        <input type="file" name="image4" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image5']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image5']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image3</label>
                        <input type="file" name="image5" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image6']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image6']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image4</label>
                        <input type="file" name="image6" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image7']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image7']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image5</label>
                        <input type="file" name="image7" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image8']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image8']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image6</label>
                        <input type="file" name="image8" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image9']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image9']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image7</label>
                        <input type="file" name="image9" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image10']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image10']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image8</label>
                        <input type="file" name="image10" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image11']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image11']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image9</label>
                        <input type="file" name="image11" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image12']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image12']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image10</label>
                        <input type="file" name="image12" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image13']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image13']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Image11</label>
                        <input type="file" name="image13" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image14']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/HomePage/'.$home['image14']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Payment Cart Image</label>
                        <input type="file" name="image14" class="form-control-file" accept="image/*">
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
@endsection
