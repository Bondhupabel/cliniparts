@extends('backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="card-title mt-2">Add News</h4>
                    <p class="text-right">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addNews">Add News</button>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table_id">
                                <thead class="">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Category Name</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($newses as $news)
                                    <tr>
                                        <th scope="row">{{ $i++ }}</th>
                                        <td><b>{{ $news->getCategory['name'] }}
                                        <td>@if($news->image)
                                                <a target="_blank" href="{{ asset('assets/backend/images/News/'.$news->image) }}">
                                                    <img src="{{ asset('assets/backend/images/News/'.$news->image) }}" style="max-height: 50px;">
                                                </a>@endif
                                        </td>
                                        <td><b>{{ $news->title }}
                                        <td><b>{!! Str::limit($news->des, 60) !!}
                                        <td><b>@if($news->status == 1) <button class="btn btn-success">Active</button> @else <button class="btn btn-warning">Inactive</button>  @endif</td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="#editNews-{{ $news->id }}" data-toggle="modal" class="btn btn-primary btn-lg">Edit</a>
                                                <span class="ml-2"></span>
                                                <a href="#deleteNews-{{ $news->id }}" data-toggle="modal" class="btn btn-danger btn-lg">Delete</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <div id="deleteNews-{{ $news->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"><i class="fa fa-trash text-danger" aria-hidden="true"></i></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('delete.news') }}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <p>Are you want to delete this?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="id" value="{{ $news->id }}">
                                                            <button type="submit" class="btn btn-danger btn-lg">Delete</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="editNews-{{ $news->id }}" class="modal fade">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edit News</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form name="editNews-{{ $news->id }}" action="{{ route('update.news') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label class="control-label">Category Name<span class="text-danger">*</span></label>
                                                            <select name="category_id" class="form-control select2">
                                                                <option selected disabled>---Select Category---</option>
                                                                @foreach($categories as $category)
                                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        @if(($news->image))
                                                            <img src="{{ asset('assets/backend/images/News/'.$news->image) }}" class="m-3" style="max-height: 50px;max-width: 80px">
                                                        @endif
                                                        <div class="form-group">
                                                            <label class="control-label">Image <span class="text-danger">*</span></label>
                                                            <input type="file" name="image" class="form-control" accept="image/*">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Title<span class="text-danger">*</span></label>
                                                            <input type="text" name="title" value="{{ $news->title }}" class="form-control">
                                                            <input type="hidden" name="id" value="{{ $news->id }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Description<span class="text-danger"></span></label>
                                                            <textarea id="editor" name="des" class="form-control">{{ $news->des }}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Status</label>
                                                            <select name="status" class="form-control">
                                                                @if($news->status == 1)
                                                                    <option value="1" class="form-control">Active</option>
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                @else
                                                                    <option value="0" class="form-control">Inactive</option>
                                                                    <option value="1" class="form-control">Active</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary btn-lg">Update Save</button>
                                                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        document.forms['editNews-{{ $news->id }}'].elements['category_id'].value = '{{ $news->getCategory['id'] }}';
                                    </script>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addNews" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add News</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('save.news') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Category Name<span class="text-danger">*</span></label>
                            <select name="category_id" class="form-control select2">
                                <option selected disabled>---Select Category---</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Image <span class="text-danger">*</span></label>
                            <input type="file" name="image" value="{{ old('image') }}" class="form-control" placeholder=" Image">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Title<span class="text-danger">*</span></label>
                            <input type="text" name="title" value="{{ old('title') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description<span class="text-danger"></span></label>
                            <textarea id="editor" name="des" class="form-control">{{ old('des') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <select name="status" class="form-control">
                                <option value="1" class="form-control">Active</option>
                                <option value="0" class="form-control">Inactive</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
