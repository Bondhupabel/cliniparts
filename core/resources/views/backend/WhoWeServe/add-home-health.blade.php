@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Home Health & Hospice</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Home Health & Hospice</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form action="{{ route('update.home.health') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image1']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$home['image1']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner One</label>
                        <input type="file" name="image1" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image2']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$home['image2']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner Two</label>
                        <input type="file" name="image2" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title1 <span class="text-danger">*</span></label>
                        <input type="text" name="title1" value="{{ $home['title1'] }}" placeholder="" class="form-control">
                        <input type="hidden" name="id" value="{{ $home['id'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title2 <span class="text-danger">*</span></label>
                        <input type="text" name="title2" value="{{ $home['title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Main Title <span class="text-danger">*</span></label>
                        <textarea id="editor" name="m_title" placeholder="" class="form-control">{{ $home['m_title'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image3']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$home['image3']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner One</label>
                        <input type="file" name="image3" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Main Description <span class="text-danger">*</span></label>
                        <textarea name="m_des" placeholder="" class="form-control">{{ $home['m_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title1 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title1" value="{{ $home['s_title1'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description1 <span class="text-danger">*</span></label>
                        <textarea name="s_des1" placeholder="" class="form-control">{{ $home['s_des1'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title2 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title2" value="{{ $home['s_title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description2 <span class="text-danger">*</span></label>
                        <textarea name="s_des2" placeholder="" class="form-control">{{ $home['s_des2'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title3 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title3" value="{{ $home['s_title3'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description3 <span class="text-danger">*</span></label>
                        <textarea name="s_des3" placeholder="" class="form-control">{{ $home['s_des3'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title4 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title4" value="{{ $home['s_title4'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description4 <span class="text-danger">*</span></label>
                        <textarea name="s_des4" placeholder="" class="form-control">{{ $home['s_des4'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title5 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title5" value="{{ $home['s_title5'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description5 <span class="text-danger">*</span></label>
                        <textarea name="s_des5" placeholder="" class="form-control">{{ $home['s_des5'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($home['image4']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$home['image4']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Bottom Banner</label>
                        <input type="file" name="image4" class="form-control-file" accept="image/*">
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
@endsection
