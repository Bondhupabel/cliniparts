@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Long Term Care</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Long Term Care</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <form action="{{ route('update.long.term') }}" method="POST" class="new-added-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($long['image1']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$long['image1']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner One</label>
                        <input type="file" name="image1" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($long['image2']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$long['image2']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner Two</label>
                        <input type="file" name="image2" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title1 <span class="text-danger">*</span></label>
                        <input type="text" name="title1" value="{{ $long['title1'] }}" placeholder="" class="form-control">
                        <input type="hidden" name="id" value="{{ $long['id'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Title2 <span class="text-danger">*</span></label>
                        <input type="text" name="title2" value="{{ $long['title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Main Title <span class="text-danger">*</span></label>
                        <textarea id="editor" name="m_title" placeholder="" class="form-control">{{ $long['m_title'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($long['image3']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$long['image3']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Banner One</label>
                        <input type="file" name="image3" class="form-control-file" accept="image/*">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Main Description <span class="text-danger">*</span></label>
                        <textarea name="m_des" placeholder="" class="form-control">{{ $long['m_des'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title1 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title1" value="{{ $long['s_title1'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description1 <span class="text-danger">*</span></label>
                        <textarea name="s_des1" placeholder="" class="form-control">{{ $long['s_des1'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title2 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title2" value="{{ $long['s_title2'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description2 <span class="text-danger">*</span></label>
                        <textarea name="s_des2" placeholder="" class="form-control">{{ $long['s_des2'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title3 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title3" value="{{ $long['s_title3'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description3 <span class="text-danger">*</span></label>
                        <textarea name="s_des3" placeholder="" class="form-control">{{ $long['s_des3'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title4 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title4" value="{{ $long['s_title4'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description4 <span class="text-danger">*</span></label>
                        <textarea name="s_des4" placeholder="" class="form-control">{{ $long['s_des4'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Title5 <span class="text-danger">*</span></label>
                        <input type="text" name="s_title5" value="{{ $long['s_title5'] }}" placeholder="" class="form-control">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        <label>Small Description5 <span class="text-danger">*</span></label>
                        <textarea name="s_des5" placeholder="" class="form-control">{{ $long['s_des5'] }}</textarea>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-12 form-group">
                        @if(($long['image4']))
                            <div class="card-block">
                                <img class="card-img-top" src="{{ asset('assets/backend/images/WhoWeServe/'.$long['image4']) }}" style="max-height: 100px;max-width: 100px;" alt="Image">
                            </div>
                        @endif
                        <label class="text-dark-medium">Bottom Banner</label>
                        <input type="file" name="image4" class="form-control-file" accept="image/*">
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
@endsection
