@extends('backend.master')
@section('content')
    <div class="breadcrumbs-area">
        <h3>Product View</h3>
        <ul>
            <li>
                <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            <li>Product Details</li>
        </ul>
    </div>
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title">
                    <h3>About {{ $product->name }}</h3>
                </div>
            </div>
            <div class="single-info-details">
                <div class="item-img">
                    <img src="{{ asset('assets/backend/images/Product/'.$product->image1) }}" alt="Product" style="max-height: 200px;min-width: 200px">
                </div>
                <div class="item-content">
                    <div class="header-inline item-header">
                        <h3 class="text-dark-medium font-medium">{{ $product->name }}</h3>
                        <div class="header-elements">
                            <ul>
                                <li><a href="{{ route('edit.product', ['id' => $product->id]) }}" title="Edit Product"><i class="far fa-edit"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="info-table table-responsive">
                        <table class="table text-nowrap">
                            <tbody>
                            <tr class="border">
                                <td>Category:</td>
                                <td class="font-medium text-dark-medium">{{ $product->subSubCategoryName->subCategoryName->categoryName['name'] }}</td>
                            </tr>
                            <tr class="border">
                                <td>Sub Category:</td>
                                <td class="font-medium text-dark-medium">{{ $product->subSubCategoryName->subCategoryName['name'] }}</td>
                            </tr>
                            <tr class="border">
                                <td>Sub Sub Category:</td>
                                <td class="font-medium text-dark-medium">{{ $product->subSubCategoryName['name'] }}</td>
                            </tr>
                            <tr class="border">
                                <td>Top Title:</td>
                                <td class="font-medium text-dark-medium">{{ $product->top_title }}</td>
                            </tr>
                            <tr class="border">
                                <td>Price:</td>
                                <td class="font-medium text-dark-medium">{{ $product->price }}</td>
                            </tr>
                            <tr class="border">
                                <td>Previous Price:</td>
                                <td class="font-medium text-dark-medium">{{ $product->old_price }}</td>
                            </tr>
                            <tr class="border">
                                <td>Image 2:</td>
                                <td class="font-medium text-dark-medium">
                                    @if($product->image2)
                                        <a href="{{ asset('assets/backend/images/Product/'.$product->image2) }}">
                                            <img src="{{ asset('assets/backend/images/Product/'.$product->image2) }}" alt="Product" style="max-height: 80px;min-width: 80px">
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            <tr class="border">
                                <td>Image 3:</td>
                                <td class="font-medium text-dark-medium">
                                    @if($product->image3)
                                        <a href="{{ asset('assets/backend/images/Product/'.$product->image3) }}">
                                            <img src="{{ asset('assets/backend/images/Product/'.$product->image3) }}" alt="Product" style="max-height: 80px;min-width: 80px">
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            <tr class="border">
                                <td>Image 4:</td>
                                <td class="font-medium text-dark-medium">
                                    @if($product->image4)
                                        <a href="{{ asset('assets/backend/images/Product/'.$product->image4) }}">
                                            <img src="{{ asset('assets/backend/images/Product/'.$product->image4) }}" alt="Product" style="max-height: 80px;min-width: 80px">
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            <tr class="border">
                                <td>Product Color:</td>
                                <td class="font-medium text-dark-medium">
                                @if($product->color_id)
                                        @foreach(json_decode($product->color_id, true) as $proId)
                                            @foreach($colors as $col_id)
                                                @if($proId == $col_id->id)
                                                    {{ $col_id->name }},
                                                @endif
                                            @endforeach
                                        @endforeach
                                @endif
                                </td>
                            </tr>
                            <tr class="border">
                                <td>Code:</td>
                                <td class="font-medium text-dark-medium">{{ $product->product_code }}</td>
                            </tr>
                            <tr class="border">
                                <td>Available:</td>
                                <td class="font-medium text-dark-medium">{{ $product->amount }}</td>
                            </tr>
                            <tr class="border">
                                <td>Short Des.:</td>
                                <td class="font-medium text-dark-medium">{{ $product->s_des }}</td>
                            </tr>
                            <tr class="border">
                                <td>Created By:</td>
                                <td class="font-medium text-dark-medium">{{ $product->getUserCreate['name'] }}</td>
                            </tr>
                            @if($product->updated_by)
                                <tr class="border">
                                    <td>Updated By:</td>
                                    <td class="font-medium text-dark-medium">{{ $product->getUserUpdate['name'] }}</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div class="" style="width: 700px;">
                            <h3>Description : </h3>
                            <p>{!! $product->des !!}</p>
                        </div>
                        <div class="" style="width: 700px;">
                            <h3>Specification : </h3>
                            <p>{!! $product->specification !!}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Student Table Area End Here -->
@endsection

