@extends('frontend.master')

@section('title')
    Blog :: Cliniparts
@endsection

@section('content')
    <!-- Page Banner Section Start -->
    <div class="page-banner-section section">
        <div class="page-banner-wrap row row-0 d-flex align-items-center ">

            <!-- Page Banner -->
            <div class="col-lg-4 col-12 order-lg-2 d-flex align-items-center justify-content-center">
                <div class="page-banner">
                    <h1>{{ $blogPage['title1'] }}</h1>
                    <p>similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita</p>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{ route('/') }}">HOME</a></li>
                            <li><a href="">{{ $blogPage['title1'] }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-1">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/Blog/'.$blogPage['image1']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-3">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/Blog/'.$blogPage['image2']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

        </div>
    </div><!-- Page Banner Section End -->

    <!-- Blog Section Start -->
    <div class="blog-section section mt-90 mb-50">
        <div class="container">
            <div class="row row-40">

                <div class="col-lg-8 col-12 order-1 order-lg-2 mb-40">
                    <div class="row">

                        <!-- Blog Item -->
                        @foreach($blogs as $blog)
                        <div class="col-12 mb-40">
                            <div class="ee-blog">
                                <a href="{{ route('blog.details', ['id'=>$blog->id]) }}" class="image">
                                    <img src="{{ asset('assets/backend/images/Blog/'.$blog['image']) }}" alt="Blog Image" style="max-height: 400px">
                                </a>
                                <div class="content text-blue">
                                    <h3><a href="{{ route('blog.details', ['id'=>$blog->id]) }}" class="text-bluee">{{ $blog['title'] }}</a></h3>
                                    <ul class="meta">
                                        <li><a href="#" class="text-blue">{{ $blog->authorName['name'] }}</a></li>
                                        <li><a href="#" class="text-blue">{{ $blog->created_at->format('d M Y') }}</a></li>
                                        <li><a href="#" class="text-blue">Comments 05</a></li>
                                    </ul>
                                    <p>{!! Str::limit($blog['des'], 200) !!}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    </div>

                    <!-- Pagination -->
                    <div class="row">
                        <div class="col">

                            <ul class="pagination">
                                {{ $blogs->links() }}
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-12 order-2 order-lg-1">

                    <!-- Blog Sidebar -->
                    <div class="blog-sidebar mb-40">

                        <h4 class="title">{{ $blogPage['title3'] }}</h4>

                        <ul>
                            @foreach($categories as $category)
                            <li><a href="{{ route('category.blog', ['id'=>$category->id]) }}">{{ $category['name'] }}</a></li>
                            @endforeach
                        </ul>

                    </div>

                    <!-- Blog Sidebar -->
                    <div class="blog-sidebar mb-40">

                        <h4 class="title">RECENT POST</h4>

                        @foreach($recentBlogs as $recent)
                        <div class="sidebar-blog">
                            <a href="{{ route('blog.details', ['id'=>$recent->id]) }}" class="image">
                                <img src="{{ asset('assets/backend/images/Blog/'.$recent['image']) }}" alt="Sidebar Blog">
                            </a>
                            <div class="content">
                                <h5><a href="{{ route('blog.details', ['id'=>$recent->id]) }}">{{ $recent['title'] }}</a></h5>
                                <span>{{ $recent->created_at->format('d M Y') }}</span>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <!-- Blog Sidebar -->
                    <div class="blog-sidebar mb-40">

                        <div class="banner">
                            <a href="{{ asset('assets/backend/images/Blog/'.$blogPage['image3']) }}">
                                <img src="{{ asset('assets/backend/images/Blog/'.$blogPage['image3']) }}" alt="Banner">
                            </a>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div><!-- Blog Section End -->
@endsection
