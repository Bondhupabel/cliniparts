@extends('frontend.master')

@section('title')
    Tech Tips :: Cliniparts
@endsection

@section('content')
    <!-- Page Banner Section Start -->
    <div class="page-banner-section section">
        <div class="page-banner-wrap row row-0 d-flex align-items-center ">

            <!-- Page Banner -->
            <div class="col-lg-4 col-12 order-lg-2 d-flex align-items-center justify-content-center">
                <div class="page-banner">
                    <h1>{{ $tech['title1'] }}</h1>
                    <p>similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita</p>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{ route('/') }}">HOME</a></li>
                            <li><a href="">{{ $tech['title1'] }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-1">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/TechTips/'.$tech['image1']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-3">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/TechTips/'.$tech['image2']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

        </div>
    </div><!-- Page Banner Section End -->

    <!-- About Section Start -->
    <div class="about-section section mt-90 mb-30">
        <div class="container">

            <div class="row row-30 mb-40">

                <!-- About Image -->
                <div class="about-image col-lg-6 mb-50">
                    <img src="{{ asset('assets/backend/images/TechTips/'.$tech['image3']) }}" alt="">
                </div>

                <!-- About Content -->
                <div class="about-content col-lg-6">
                    <div class="row">
                        <div class="col-12 mb-50">
                            <p class="text-blue">{!! $tech['des1'] !!}</p>
                        </div>

                    </div>
                </div>

            </div>

            <div class="row mb-30">

                <!-- About Feature -->
                <div class="about-feature col-lg-7">
                    <div class="row">

                        <div class="col-md-12 mb-30">
                            <p class="text-blue">{!! $tech['des2'] !!}</p>
                        </div>

                    </div>
                </div>

                <!-- About Feature Banner -->
                <div class="about-feature-banner col-lg-5 col-12 mb-30">
                    <div class="banner">
                        <a href="{{ asset('assets/backend/images/TechTips/'.$tech['image4']) }}">
                            <img src="{{ asset('assets/backend/images/TechTips/'.$tech['image4']) }}" alt="Banner">
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </div><!-- About Section End -->
@endsection
