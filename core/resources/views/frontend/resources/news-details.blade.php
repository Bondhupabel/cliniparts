@extends('frontend.master')

@section('title')
    News Details :: Cliniparts
@endsection

@section('content')
    <!-- Page Banner Section Start -->
    <div class="page-banner-section section">
        <div class="page-banner-wrap row row-0 d-flex align-items-center ">

            <!-- Page Banner -->
            <div class="col-lg-4 col-12 order-lg-2 d-flex align-items-center justify-content-center">
                <div class="page-banner">
                    <h1>{{ $newsPage['title1'] }}</h1>
                    <p>similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita</p>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{ route('/') }}">HOME</a></li>
                            <li><a href="">{{ $newsPage['title1'] }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-1">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/News/'.$newsPage['image1']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-3">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/News/'.$newsPage['image2']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

        </div>
    </div><!-- Page Banner Section End -->

    <!-- Blog Section Start -->
    <div class="blog-section section mt-90 mb-50">
        <div class="container">
            <div class="row row-40">

                <div class="col-lg-8 col-12 order-1 order-lg-2 mb-40">
                    <div class="row">

                        <!-- Blog Item -->
                        <div class="col-12 mb-40">
                            <div class="ee-blog">
                                <a href="{{ asset('assets/backend/images/News/'.$news['image']) }}" class="image">
                                    <img src="{{ asset('assets/backend/images/News/'.$news['image']) }}" alt="Blog Image" style="max-height: 400px">
                                </a>
                                <div class="content text-blue">
                                    <h3><a href="" class="text-bluee">{{ $news['title'] }}</a></h3>
                                    <ul class="meta">
                                        <li><a href="#" class="text-blue">{{ $news->authorName['name'] }}</a></li>
                                        <li><a href="#" class="text-blue">{{ $news->created_at->format('d M Y') }}</a></li>
                                        <li><a href="#" class="text-blue">Comments 05</a></li>
                                    </ul>
                                    <p>{!! $news['des'] !!}</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- Pagination -->
                    <div class="row">
                        <div class="col">

                            <ul class="pagination">
                                <li><a href="#" class="text-blue">Back</a></li>
                                <li><a href="#" class="text-blue">1</a></li>
                                <li class="active"><a href="#" class="text-blue">2</a></li>
                                <li><a href="#" class="text-blue">3</a></li>
                                <li> - - - - - </li>
                                <li><a href="#" class="text-blue">18</a></li>
                                <li><a href="#" class="text-blue">18</a></li>
                                <li><a href="#" class="text-blue">20</a></li>
                                <li><a href="#" class="text-blue">Next</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-12 order-2 order-lg-1">

                    <!-- Blog Sidebar -->
                    <div class="blog-sidebar mb-40">

                        <h4 class="title">{{ $newsPage['title3'] }}</h4>

                        <ul>
                            @foreach($categories as $category)
                                <li><a href="{{ route('category.news', ['id'=>$category->id]) }}">{{ $category['name'] }}</a></li>
                            @endforeach
                        </ul>

                    </div>

                    <!-- Blog Sidebar -->
                    <div class="blog-sidebar mb-40">

                        <h4 class="title">RECENT POST</h4>

                        @foreach($recentNewses as $recent)
                            <div class="sidebar-blog">
                                <a href="{{ route('news.details', ['id'=>$recent->id]) }}" class="image">
                                    <img src="{{ asset('assets/backend/images/News/'.$recent['image']) }}" alt="Sidebar Blog">
                                </a>
                                <div class="content">
                                    <h5><a href="{{ route('news.details', ['id'=>$recent->id]) }}">{{ $recent['title'] }}</a></h5>
                                    <span>{{ $recent->created_at->format('d M Y') }}</span>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <!-- Blog Sidebar -->
                    <div class="blog-sidebar mb-40">

                        <div class="banner">
                            <a href="{{ asset('assets/backend/images/News/'.$newsPage['image3']) }}">
                                <img src="{{ asset('assets/backend/images/News/'.$newsPage['image3']) }}" alt="Banner">
                            </a>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div><!-- Blog Section End -->
@endsection
