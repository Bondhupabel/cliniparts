@extends('frontend.master')

@section('title')
    About Us :: Cliniparts
@endsection

@section('content')
    <!-- Page Banner Section Start -->
    <div class="page-banner-section section">
        <div class="page-banner-wrap row row-0 d-flex align-items-center ">

            <!-- Page Banner -->
            <div class="col-lg-4 col-12 order-lg-2 d-flex align-items-center justify-content-center">
                <div class="page-banner">
                    <h1>{{ $about['title1'] }}</h1>
                    <p>similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita</p>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="{{ route('/') }}">HOME</a></li>
                            <li><a href="">{{ $about['title1'] }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-1">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/About/'.$about['image1']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

            <!-- Banner -->
            <div class="col-lg-4 col-md-6 col-12 order-lg-3">
                <div class="banner">
                    <a href="">
                        <img src="{{ asset('assets/backend/images/About/'.$about['image2']) }}" alt="Banner" style="max-height: 200px">
                    </a>
                </div>
            </div>

        </div>
    </div><!-- Page Banner Section End -->

    <!-- About Section Start -->
    <div class="about-section section mt-90 mb-30">
        <div class="container">

            <div class="row row-30 mb-40">

                <!-- About Image -->
                <div class="about-image col-lg-6 mb-50">
                    <img src="{{ asset('assets/backend/images/About/'.$about['image3']) }}" alt="">
                </div>

                <!-- About Content -->
                <div class="about-content col-lg-6">
                    <div class="row">
                        <div class="col-12 mb-50">
                            <h1 class="text-blue">{{ $about['wc_title1'] }} <span>{{ $about['wc_title2'] }}</span></h1>
                            <p class="text-blue">{{ $about['wc_des'] }}</p>
                        </div>

                        <div class="col-12 mb-50">
                            <h4 class="text-blue">{{ $about['we_title'] }}</h4>
                            <p class="text-blue">{{ $about['we_des'] }}</p>
                        </div>

                        <div class="col-12 mb-50">
                            <h4 class="text-blue">{{ $about['win_title'] }}</h4>
                            <p class="text-blue">{{ $about['win_des'] }}</p>
                        </div>

                    </div>
                </div>

            </div>

            <div class="row row-10 mb-60">

                <!-- Banner -->
                <div class="col-md-3 mb-20">
                    <div class="banner">
                        <a href="{{ asset('assets/backend/images/About/'.$about['image4']) }}">
                            <img src="{{ asset('assets/backend/images/About/'.$about['image4']) }}" alt="Banner">
                        </a>
                    </div>
                </div>

                <!-- Banner -->
                <div class="col-md-9">
                    <div class="row row-10">

                        <div class="col-md-6 col-12 mb-20">
                            <div class="banner">
                                <a href="{{ asset('assets/backend/images/About/'.$about['image5']) }}">
                                    <img src="{{ asset('assets/backend/images/About/'.$about['image5']) }}" alt="Banner">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mb-20">
                            <div class="banner">
                                <a href="{{ asset('assets/backend/images/About/'.$about['image6']) }}">
                                    <img src="{{ asset('assets/backend/images/About/'.$about['image6']) }}" alt="Banner">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- Mission, Vission & Goal -->
            <div class="about-mission-vission-goal row row-20 mb-40">

                <div class="col-lg-4 col-md-6 col-12 mb-40">
                    <h3 class="text-blue">{{ $about['vision_title'] }}</h3>
                    <p class="text-blue">{{ $about['vision_des'] }}</p>
                </div>

                <div class="col-lg-4 col-md-6 col-12 mb-40">
                    <h3 class="text-blue">{{ $about['mission_title'] }}</h3>
                    <p class="text-blue">{{ $about['mission_des'] }}</p>
                </div>

                <div class="col-lg-4 col-md-6 col-12 mb-40">
                    <h3 class="text-blue">{{ $about['goal_title'] }}</h3>
                    <p class="text-blue">{{ $about['goal_des'] }}</p>
                </div>

            </div>

            <div class="row mb-30">

                <!-- About Section Title -->
                <div class="about-section-title col-12 mb-50">
                    <h3 class="text-blue">{{ $about['you_title'] }}</h3>
                    <p class="text-blue">{{ $about['you_des'] }}</p>
                </div>

                <!-- About Feature -->
                <div class="about-feature col-lg-7 col-12">
                    <div class="row">

                        <div class="col-md-6 col-12 mb-30">
                            <h4 class="text-blue">{{ $about['fast_title'] }}</h4>
                            <p class="text-blue">{{ $about['fast_des'] }}</p>
                        </div>

                        <div class="col-md-6 col-12 mb-30">
                            <h4 class="text-blue">{{ $about['quality_title'] }}</h4>
                            <p class="text-blue">{{ $about['quality_des'] }}</p>
                        </div>

                        <div class="col-md-6 col-12 mb-30">
                            <h4 class="text-blue">{{ $about['secure_title'] }}</h4>
                            <p class="text-blue">{{ $about['secure_des'] }}</p>
                        </div>

                        <div class="col-md-6 col-12 mb-30">
                            <h4 class="text-blue">{{ $about['money_title'] }}</h4>
                            <p class="text-blue">{{ $about['money_des'] }}</p>
                        </div>

                        <div class="col-md-6 col-12 mb-30">
                            <h4 class="text-blue">{{ $about['easy_title'] }}</h4>
                            <p class="text-blue">{{ $about['easy_des'] }}</p>
                        </div>

                        <div class="col-md-6 col-12 mb-30">
                            <h4 class="text-blue">{{ $about['free_title'] }}</h4>
                            <p class="text-blue">{{ $about['free_des'] }}</p>
                        </div>

                        <div class="col-md-6 col-12 mb-30">
                            <h4 class="text-blue">{{ $about['support_title'] }}</h4>
                            <p class="text-blue">{{ $about['support_des'] }}</p>
                        </div>

                    </div>
                </div>

                <!-- About Feature Banner -->
                <div class="about-feature-banner col-lg-5 col-12 mb-30">
                    <div class="banner">
                        <a href="{{ asset('assets/backend/images/About/'.$about['image7']) }}">
                            <img src="{{ asset('assets/backend/images/About/'.$about['image7']) }}" alt="Banner">
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </div><!-- About Section End -->
@endsection
