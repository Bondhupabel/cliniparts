<!-- Subscribe Section Start -->
<div class="subscribe-section section bg-gray pt-55 pb-55">
    <div class="container">
        <div class="row align-items-center">

            <!-- Mailchimp Subscribe Content Start -->
            <div class="col-lg-6 col-12 mb-15 mt-15">
                <div class="subscribe-content">
                    <h2>SUBSCRIBE <span>OUR NEWSLETTER</span></h2>
                    <h2><span>TO GET LATEST</span> PRODUCT UPDATE</h2>
                </div>
            </div><!-- Mailchimp Subscribe Content End -->


            <!-- Mailchimp Subscribe Form Start -->
            <div class="col-lg-6 col-12 mb-15 mt-15">

                <form class="subscribe-form" action="{{ route('send.subscribe') }}" method="POST">
                    @csrf
                    <input type="email" name="email" autocomplete="off" placeholder="Enter your email here" required/>
                    <button >subscribe</button>
                </form>
                <!-- mailchimp-alerts Start -->
                <div class="mailchimp-alerts text-centre">
                    <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                    <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                    <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                </div><!-- mailchimp-alerts end -->

            </div><!-- Mailchimp Subscribe Form End -->

        </div>
    </div>
</div>
<!-- Subscribe Section End -->


<!-- Footer Section Start -->
<div class="footer-section section bg-ivory">

    <!-- Footer Top Section Start -->
    <div class="footer-top-section section pt-90 pb-50">
        <div class="container">
            <div class="row">

                <!-- Footer Widget Start -->
                <div class="col-lg-3 col-md-6 col-12 mb-40">
                    <div class="footer-widget">

                        <h4 class="widget-title">{{ $homeText['name6'] }}</h4>

                        <p class="contact-info">
                            {!! $homeText['contact'] !!}
                        </p>

                    </div>
                </div><!-- Footer Widget End -->

                <!-- Footer Widget Start -->
                <div class="col-lg-3 col-md-6 col-12 mb-40">
                    <div class="footer-widget">

                        <h4 class="widget-title">{{ $homeText['name7'] }}</h4>

                        <ul class="link-widget">
                            <li><a href="shop-view.html">Batteries</a></li>
                            <li><a href="shop-view.html">Biomedical Parts</a></li>
                            <li><a href="shop-view.html">Cables & Sensors</a></li>
                            <li><a href="shop-view.html">Laboratory Parts</a></li>
                            <li><a href="shop-view.html">Lighting & Bulbs</a></li>
                            <li><a href="shop-view.html">Medical Imaging Parts</a></li>
                        </ul>

                    </div>
                </div><!-- Footer Widget End -->

                <!-- Footer Widget Start -->
                <div class="col-lg-3 col-md-6 col-12 mb-40">
                    <div class="footer-widget">

                        <h4 class="widget-title">{{ $homeText['name8'] }}</h4>

                        <ul class="link-widget">
                            <li><a href="dashboard.html">My Account</a></li>
                            <li><a href="orders.html">Orders</a></li>
                            <li><a href="orders.html">Order History</a></li>
                            <li><a href="track-order.html">Track Order</a></li>
                        </ul>

                    </div>
                </div><!-- Footer Widget End -->

                <!-- Footer Widget Start -->
                <div class="col-lg-3 col-md-6 col-12 mb-40">
                    <div class="footer-widget">

                        <h4 class="widget-title">{{ $homeText['name9'] }}</h4>

                        <ul class="link-widget">
                            <li><a href="about-us.html">Company Profile</a></li>
                            <li><a href="contact.html">Customer Support</a></li>
                            <li><a href="blog.html">Advertise with Us- COMING SOON</a></li>
                            <li><a href="terms-conditions.html">Terms & Conditions</a></li>
                        </ul>

                    </div>
                </div><!-- Footer Widget End -->

            </div>

        </div>
    </div><!-- Footer Bottom Section Start -->

    <!-- Footer Bottom Section Start -->
    <div class="footer-bottom-section section">
        <div class="container">
            <div class="row">

                <!-- Footer Copyright -->
                <div class="col-lg-6 col-12">
                    <div class="footer-copyright"><p>&copy; Copyright, {{ \Carbon\Carbon::now('Y') }} All Rights Reserved by <a href="http://inoventivebd.com/">Inoventice BD</a></p></div>
                </div>

                <!-- Footer Payment Support -->
                <div class="col-lg-6 col-12">
                    <div class="footer-payments-image"><img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image14) }}" alt="Payment Support Image"></div>
                </div>

            </div>
        </div>
    </div><!-- Footer Bottom Section Start -->

</div>
<!-- Footer Section End -->
