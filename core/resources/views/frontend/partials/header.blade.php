<!-- Header Section Start -->
<div class="header-section section">

    <!-- Header Top Start -->
    <div class="header-top header-top-one header-top-border pt-10 pb-10">
        <div class="container">
            <div class="row align-items-center justify-content-between">

                <div class="col mt-5 mb-5">
                    <!-- Header Links Start -->
                    <div class="header-links">
                        <a href="">
                            <img src="{{ asset('assets/frontend/images/icons/car.png') }}" alt="Car Icon"> <span class="text-blue">{{ $homeText['name1'] }}</span>
                        </a>
                    </div><!-- Header Links End -->
                </div>

                <div class="col order-12 order-xs-12 order-lg-2 mt-5 mb-5">
                    <!-- Header Advance Search Start -->
                    <div class="header-advance-search">

                        <form action="#">
                            <div class="input ">
                                <input type="text" placeholder="Search Keyword or Item Number">
                            </div>

                            <div class="submit">
                                <button><i class="icofont icofont-search-alt-1"></i></button>
                            </div>
                        </form>

                    </div><!-- Header Advance Search End -->
                </div>

                <div class="col order-2 order-xs-2 order-lg-12 mt-5 mb-5">
                    <!-- Header Account Links Start -->
                    <div class="header-account-links text-blue">
                        <a href=""><i class="icofont icofont-user-alt-7"></i> <span>my account</span></a>
                        @if(Auth::user())
                            <a  href="#" onclick="event.preventDefault(); document.getElementById('logoutForm').submit();"><i class="icofont icofont-login d-none"></i> <span>Logout</span></a>
                            <form id="logoutForm" action="{{ route('logout') }}" method="POST">
                                @csrf
                            </form>
                        @else
                            <a href="{{ route('login') }}"><i class="icofont icofont-login d-none"></i> <span>Login</span></a>
                        @endif
                    </div>
                    <!-- Header Account Links End -->
                </div>

            </div>
        </div>
    </div>
    <!-- Header Top End -->

    <!-- Header Bottom Start -->
    <div class="header-bottom header-bottom-one header-sticky">
        <div class="container">
            <div class="row align-items-center justify-content-between">

                <div class="col">
                    <!-- Logo Start -->
                    <div class="header-logo">
                        <a href="{{ route('/') }}">
                            <img src="{{ asset('assets/backend/images/HomePage/'.$homeImage->image2) }}" class="" style="max-height: 80px;min-width: 100px" alt="cliniparts">
{{--                            <h1 class="text-white">Logo</h1>--}}
                        </a>
                    </div><!-- Logo End -->
                </div>

                <div class="col order-12 order-lg-2 order-xl-2 d-none d-lg-block">
                    <!-- Main Menu Start -->
                    <div class="main-menu">
                        <nav>
                            <ul>

                                <li class="active menu-item-has-children"><a>Categories</a>
                                    <ul class="sub-menu">
                                        @foreach($categories as $category)
                                        <li class="menu-item-has-children"><a href="">{{ $category['name'] }}</a>
                                            @foreach($subCategories as $sub)
                                                @if($sub->category_id == $category->id)
                                                    {{--<ul class="sub-menu scroll">--}}
                                                        {{--<li><a href="">{{ $sub['name'] }}</a></li>--}}
                                                    {{--</ul>--}}
                                                    <ul class="sub-menu">
                                                        <li class="menu-item-has-children"><a href="">{{ $sub['name'] }}</a>
                                                            @foreach($subSubCategories as $subCat)
                                                                @if($subCat->subcategory_id == $sub->id)
                                                                    <ul class="sub-menu scroll">
                                                                        <li><a href="">{{ $subCat['name'] }}</a></li>
                                                                    </ul>
                                                                @endif
                                                            @endforeach
                                                        </li>
                                                    </ul>
                                                @endif
                                            @endforeach
                                        </li>
                                        @endforeach

                                        <li class="menu-item-has-children"><a href="">Medical Imaging Parts</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item-has-children"><a href="shop.html">Advanced & Hybrid Imaging</a>
                                                    <ul class="sub-menu scroll">
                                                        <li><a href="shop.html">C-Arms</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="">Who we Serve</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ route('show.integrated') }}">Integrated Delivery Network</a></li>
                                        <li><a href="{{ route('show.acute.care') }}">Acute Care</a></li>
                                        <li><a href="{{ route('show.surgery') }}">Surgery Centers</a></li>
                                        <li><a href="{{ route('show.long.term') }}">Long-Term Care</a></li>
                                        <li><a href="{{ route('show.education') }}">Education and Research</a></li>
                                        <li><a href="{{ route('show.health') }}">Home Health and Hospice</a></li>
                                        <li><a href="{{ route('show.physician') }}">Physician Offices</a></li>
                                        <li><a href="{{ route('show.assisted') }}">Assisted Living</a></li>
                                        <li><a href="{{ route('show.managed') }}">Managed Care</a></li>
                                        <li><a href="{{ route('show.independent') }}">Independent Service Organizations</a></li>
                                    </ul>
                                </li>

                                <li><a href="">Browsing History</a></li>

                                <li class="menu-item-has-children"><a href="#">Resources</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ route('show.tech.tips') }}">Tech Tips</a></li>
                                        <li><a href="{{ route('show.blog') }}">Blog</a></li>
                                        <li><a href="{{ route('show.news') }}">News Room</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('show.about') }}">About Us</a></li>
                                <li><a href="{{ route('show.contact') }}">CONTACT</a></li>
                            </ul>
                        </nav>
                    </div><!-- Main Menu End -->
                </div>

                <div class="col order-2 order-lg-12 order-xl-12">
                    <!-- Header Shop Links Start -->
                    <div class="header-shop-links">

                        <!-- Compare -->
                        <a href="" class="header-compare"><i class="ti-control-shuffle"></i></a>
                        <!-- Wishlist -->
                        <a href="" class="header-wishlist"><i class="ti-heart"></i> <span class="number">3</span></a>
                        <!-- Cart -->
                        <a href="javascript:void(0)"  class="header-cart"><i class="ti-shopping-cart"></i> <span class="number" id="cartProductsCount"> 0 </span></a>

                    </div><!-- Header Shop Links End -->
                </div>

                <!-- Mobile Menu -->
                <div class="mobile-menu order-12 d-block d-lg-none col"></div>

            </div>
        </div>
    </div>
    <!-- Header Bottom End -->


</div>
<!-- Header Section End -->
<!-- Mini Cart Wrap Start -->
<div class="mini-cart-wrap">

    <!-- Mini Cart Top -->
    <div class="mini-cart-top">

        <button class="close-cart">Close Cart<i class="icofont icofont-close"></i></button>

    </div>

    <!-- Mini Cart Products -->
    <ul class="mini-cart-products" id="ProductsCart">
        <?php $total = 0 ?>
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
                <li>
                    <a class="image"><img src="{{ asset('assets/backend/images/Product/'.$details['image1']) }}" alt="Product"></a>
                    <div class="content">
                        <a href="" class="title">{{ $details['name'] }}</a>
                        <span class="price">Price: {{ $details['price'] }}</span>
                        <span class="qty">Qty:
                            <button class="bg-primary text-white" style="height: 35px;width: 35px"> - </button>
                                {{ $details['quantity'] }}
                            <button class="bg-primary text-white" style="height: 35px;width: 35px"> + </button>
                        </span>
                    </div>
                    <input type="hidden" value="{{ $total  += ( $details['price'] ) * ( $details['quantity'] ) }}">
                    <button class="remove" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
                    {{--<td class="actions" data-th="">--}}
                        {{--<button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button>--}}
                        {{--<button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>--}}
                    {{--</td>--}}
                </li>
                    <!-- Mini Cart Bottom -->
                    <div class="mini-cart-bottom">
                        <h4 class="sub-total">Total: <span>{{ number_format($total,2) }}</span></h4>
                        <div class="button">
                            <a href="{{ route('checkout') }}">CHECK OUT</a>
                        </div>
                    </div>
            @endforeach
        @endif
    </ul>

    <!-- Mini Cart Bottom -->
    {{--<div class="mini-cart-bottom">--}}
        {{--<h4 class="sub-total">Total: <span>{{ number_format($total,2) }}</span></h4>--}}
        {{--<div class="button">--}}
            {{--<a href="{{ route('checkout') }}">CHECK OUT</a>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>
<!-- Mini Cart Wrap End -->

<!-- Cart Overlay -->
<div class="cart-overlay"></div>
@push('jscripts')

@endpush
