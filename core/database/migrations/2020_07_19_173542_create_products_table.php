<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subcategory_id')->nullable();
            $table->bigInteger('sub_subcategory_id')->nullable();
            $table->longText('color_id')->nullable();
            $table->string('top_title')->nullable();
            $table->string('name')->nullable();
            $table->text('image1')->nullable();
            $table->text('image2')->nullable();
            $table->text('image3')->nullable();
            $table->text('image4')->nullable();
            $table->decimal('old_price',18,2)->nullable();
            $table->decimal('price',18,2)->nullable();
            $table->longText('s_des')->nullable();
            $table->string('product_code')->nullable();
            $table->string('amount')->nullable();
            $table->longText('des')->nullable();
            $table->longText('specification')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active, 0=inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
