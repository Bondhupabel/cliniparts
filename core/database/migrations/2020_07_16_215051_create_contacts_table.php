<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('image1')->nullable();
            $table->text('image2')->nullable();
            $table->string('title1')->nullable();
            $table->string('title2')->nullable();
            $table->string('m_title')->nullable();
            $table->string('title3')->nullable();
            $table->string('title4')->nullable();
            $table->string('title5')->nullable();
            $table->string('address')->nullable();
            $table->text('address_des')->nullable();
            $table->string('phone')->nullable();
            $table->text('phone_des')->nullable();
            $table->string('web')->nullable();
            $table->text('web_des')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
