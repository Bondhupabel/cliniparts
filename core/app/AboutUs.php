<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class AboutUs extends Model
{
    public static function updateAboutUsData($request){
        $about = AboutUs::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/About/'.$about->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $about->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/About/'.$about->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $about->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/About/'.$about->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $about->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/About/'.$about->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $about->image4 = $imageName;
        }
        if ($request->file('image5')){
            @unlink('assets/backend/images/About/'.$about->image5);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $about->image5 = $imageName;
        }
        if ($request->file('image6')){
            @unlink('assets/backend/images/About/'.$about->image6);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $about->image6 = $imageName;
        }
        if ($request->file('image7')){
            @unlink('assets/backend/images/About/'.$about->image7);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/About/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $about->image7 = $imageName;
        }
        $about->title1 = $request->title1;
        $about->title2 = $request->title2;
        $about->wc_title1 = $request->wc_title2;
        $about->wc_title2 = $request->wc_title2;
        $about->wc_des = $request->wc_des;
        $about->we_title = $request->we_title;
        $about->we_des = $request->we_des;
        $about->win_title = $request->win_title;
        $about->win_des = $request->win_des;
        $about->vision_title = $request->vision_title;
        $about->vision_des = $request->vision_des;
        $about->mission_title = $request->mission_title;
        $about->mission_des = $request->mission_des;
        $about->goal_title = $request->goal_title;
        $about->goal_des = $request->goal_des;
        $about->you_title = $request->you_title;
        $about->you_des = $request->you_des;
        $about->fast_title = $request->fast_title;
        $about->fast_des = $request->fast_des;
        $about->quality_title = $request->quality_title;
        $about->quality_des = $request->quality_des;
        $about->secure_title = $request->secure_title;
        $about->secure_des = $request->secure_des;
        $about->money_title = $request->money_title;
        $about->money_des = $request->money_des;
        $about->easy_title = $request->easy_title;
        $about->easy_des = $request->easy_des;
        $about->free_title = $request->free_title;
        $about->free_des = $request->free_des;
        $about->support_title = $request->support_title;
        $about->support_des = $request->support_des;
        $about->save();
    }
}
