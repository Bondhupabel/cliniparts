<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use App\BlogTitle;
use App\News;
use App\NewsCategory;
use App\NewsTitle;
use App\TechTip;
use Illuminate\Http\Request;
use Auth;
use Image;

class ResourcesController extends Controller
{
    //Tech Tips
    public function addTechTips(){
        $tech = TechTip::first();
        return view('backend.resources.add-tech-tips', compact('tech'));
    }
    public function updateTechTips(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        TechTip::addTechTipsData($request);
        return back()->withSuccess('Update Successful');
    }

    //Blog Title
    public function addBlogTitle(){
        $title = BlogTitle::first();
        return view('backend.resources.add-blog-title', compact('title'));
    }
    public function updateBlogTitle(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        BlogTitle::addBlogData($request);
        return back()->withSuccess('Update Successful');
    }
    //Blog Category
    public function addBlogCategory(){
        $categories = BlogCategory::orderBy('id', 'desc')->get();
        return view('backend.resources.add-blog-category', compact('categories'));
    }
    public function saveBlogCategory(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        BlogCategory::addBlogCategoryData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateBlogCategory(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        BlogCategory::updateBlogCategoryData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteBlogCategory(Request $request){
        BlogCategory::deleteBlogCategoryData($request);
        return back()->withSuccess('Delete Successful');
    }
    //Blog
    public function addBlog(){
        $categories = BlogCategory::where('status', 1)->orderBy('name', 'asc')->get();
        $blogs = Blog::orderBy('id', 'desc')->get();
        return view('backend.resources.add-blog', compact('categories', 'blogs'));
    }
    public function saveBlog(Request $request){
        $this->validate($request,[
            'category_id' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Blog::addBlogData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateBlog(Request $request){
        $this->validate($request,[
            'category_id' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Blog::updateBlogData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteBlog(Request $request){
        Blog::deleteBlogData($request);
        return back()->withSuccess('Delete Successful');
    }

    //News Room Title
    public function addNewsTitle(){
        $title = NewsTitle::first();
        return view('backend.resources.add-news-title', compact('title'));
    }
    public function updateNewsTitle(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        NewsTitle::addNewsData($request);
        return back()->withSuccess('Update Successful');
    }
    //News Category
    public function addNewsCategory(){
        $categories = NewsCategory::orderBy('id', 'desc')->get();
        return view('backend.resources.add-news-category', compact('categories'));
    }
    public function saveNewsCategory(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        NewsCategory::addNewsCategoryData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateNewsCategory(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        NewsCategory::updateNewsCategoryData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteNewsCategory(Request $request){
        NewsCategory::deleteNewsCategoryData($request);
        return back()->withSuccess('Delete Successful');
    }
    //News
    public function addNews(){
        $categories = NewsCategory::where('status', 1)->orderBy('name', 'asc')->get();
        $newses = News::orderBy('id', 'desc')->get();
        return view('backend.resources.add-news', compact('categories', 'newses'));
    }
    public function saveNews(Request $request){
        $this->validate($request,[
            'category_id' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        News::addNewsData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateNews(Request $request){
        $this->validate($request,[
            'category_id' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        News::updateNewsData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteNews(Request $request){
        News::deleteNewsData($request);
        return back()->withSuccess('Delete Successful');
    }
}
