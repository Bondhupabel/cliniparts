<?php

namespace App\Http\Controllers;

use App\AcuteCare;
use App\Assisted;
use App\Education;
use App\HomeHealth;
use App\Independent;
use App\Integrated;
use App\LongTerm;
use App\Managed;
use App\Physician;
use App\Surgery;
use Illuminate\Http\Request;
use Image;

class WhoWeServeController extends Controller
{
    //Integrated Delivery Network
    public function addIntegrated(){
        $integrated = Integrated::first();
        return view('backend.WhoWeServe.add-integrated', compact('integrated'));
    }
    public function updateIntegrated(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Integrated::addIntegratedData($request);
        return back()->withSuccess('Update Successful');
    }

    //Acute Care
    public function addAcute(){
        $acute = AcuteCare::first();
        return view('backend.WhoWeServe.add-acute', compact('acute'));
    }
    public function updateAcute(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        AcuteCare::addAcuteCareData($request);
        return back()->withSuccess('Update Successful');
    }

    //Surgery Centers
    public function addSurgery(){
        $surgery = Surgery::first();
        return view('backend.WhoWeServe.add-surgery', compact('surgery'));
    }
    public function updateSurgery(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Surgery::addSurgeryData($request);
        return back()->withSuccess('Update Successful');
    }

    //Long Term Care
    public function addLongTerm(){
        $long = LongTerm::first();
        return view('backend.WhoWeServe.add-long-term', compact('long'));
    }
    public function updateLongTerm(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        LongTerm::addLongTermData($request);
        return back()->withSuccess('Update Successful');
    }

    //Education & Research
    public function addEducation(){
        $education = Education::first();
        return view('backend.WhoWeServe.add-education', compact('education'));
    }
    public function updateEducation(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Education::addEducationData($request);
        return back()->withSuccess('Update Successful');
    }

    //Home Health and Hospice
    public function addHomeHealth(){
        $home = HomeHealth::first();
        return view('backend.WhoWeServe.add-home-health', compact('home'));
    }
    public function updateHomeHealth(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        HomeHealth::addHomeHealthData($request);
        return back()->withSuccess('Update Successful');
    }

    //Physician Offices
    public function addPhysician(){
        $physician = Physician::first();
        return view('backend.WhoWeServe.add-physician', compact('physician'));
    }
    public function updatePhysician(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Physician::addPhysicianData($request);
        return back()->withSuccess('Update Successful');
    }

    //Assisted Living
    public function addAssisted(){
        $assisted = Assisted::first();
        return view('backend.WhoWeServe.add-assisted', compact('assisted'));
    }
    public function updateAssisted(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Assisted::addAssistedData($request);
        return back()->withSuccess('Update Successful');
    }

    //Managed Care
    public function addManaged(){
        $managed = Managed::first();
        return view('backend.WhoWeServe.add-managed', compact('managed'));
    }
    public function updateManaged(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Managed::addManagedData($request);
        return back()->withSuccess('Update Successful');
    }

    //Independent Service & Organizations
    public function addIndependent(){
        $independent = Independent::first();
        return view('backend.WhoWeServe.add-independent', compact('independent'));
    }
    public function updateIndependent(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
        ]);
        Independent::addIndependentData($request);
        return back()->withSuccess('Update Successful');
    }
}
