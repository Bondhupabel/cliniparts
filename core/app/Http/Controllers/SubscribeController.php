<?php

namespace App\Http\Controllers;

use App\Subscribe;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    public function sendSubscribe(Request $request){
        $this->validate($request,[
            'email' => 'required'
        ]);
        Subscribe::create([
            'email' => $request->email,
        ]);
        return back()->with('message', 'Subscribe Successfully');
    }
    public function showSubscribeEmail(){
        $emails = Subscribe::orderBy('id', 'desc')->get();
        return view('backend.subscribe.email', compact('emails'));
    }
    public function deleteSubscribeEmail(Request $request){
        $email = Subscribe::find($request->id);
        $email->delete();
        return back()->withSuccess('Delete Successful');
    }
}
