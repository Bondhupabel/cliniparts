<?php

namespace App\Http\Controllers;

use App\Category;
use App\HomeImage;
use App\HomeSection;
use App\HomeText;
use App\PopUp;
use App\Product;
use App\Slider;
use App\SubCategory;
use App\SubSubCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $sliders = Slider::where('status', 1)->orderBy('id', 'desc')->take(5)->get();
        $products = Product::where('status', 1)->orderBy('id', 'desc')->get();
        $featured_products = Product::where('featured', 1)->where('status', 1)->orderBy('id', 'desc')->get();
        $homeSections = HomeSection::where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        $subscribe = PopUp::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.home.home', compact('homeImage', 'homeText', 'sliders', 'products', 'featured_products', 'homeSections', 'subscribe', 'categories', 'subCategories', 'subSubCategories'));
    }
}
