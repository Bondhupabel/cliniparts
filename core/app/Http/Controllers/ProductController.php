<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductColor;
use App\SubCategory;
use App\SubSubCategory;
use Illuminate\Http\Request;
use Auth;
use Image;

class ProductController extends Controller
{
    //Ajax
    public function getSubCategory(Request $request){
        $id = $request->get('id');
        $subSubCategories = SubSubCategory::where('subcategory_id', $id)->where('status', 1)->get();

        echo '<option selected disabled value="" >---SELECT SUB SUB CATEGORY---</option>';
        foreach ($subSubCategories as $subSubCategory){
            echo '<option value="'.$subSubCategory->id.'">'.$subSubCategory->name.'</option>';
        }
    }

    //Product
    public function addProduct(){
        $categories = Category::where('status', 1)->orderBy('id', 'desc')->get();
        $colors = ProductColor::where('status', 1)->orderBy('id', 'desc')->get();
        return view('backend.product.add-product', compact('categories', 'colors'));
    }
    public function saveProduct(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'image1' => 'required|mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'price' => 'required',
            'product_code' => 'required|unique:products,product_code',
        ]);
        Product::addProductData($request);
        return back()->withSuccess('Save Successful')->withInput();
    }
    public function manageProduct(){
        $products = Product::orderBy('id', 'desc')->get();
        $colors = ProductColor::where('status', 1)->orderBy('id', 'desc')->get();
        return view('backend.product.manage-product', compact('products', 'colors'));
    }
    public function viewProduct($id){
        $product = Product::find($id);
        $colors = ProductColor::where('status', 1)->orderBy('id', 'desc')->get();
        return view('backend.product.view-product', compact('product', 'colors'));
    }
    public function editProduct($id){
        $categories = Category::where('status', 1)->orderBy('id', 'desc')->get();
        $subCategories = SubCategory::where('status', 1)->orderBy('id', 'desc')->get();
        $subSubCategories = SubSubCategory::where('status', 1)->orderBy('id', 'desc')->get();
        $colors = ProductColor::where('status', 1)->orderBy('id', 'desc')->get();
        $product = Product::find($id);
        return view('backend.product.edit-product', compact('categories', 'subCategories', 'subSubCategories', 'colors', 'product'));
    }
    public function updateProduct(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image3' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image4' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'price' => 'required',
            'product_code' => 'required',
        ]);
        Product::updateProductData($request);
        return redirect()->route('manage.product')->withSuccess('Update Successful');
    }
    public function deleteProduct(Request $request){
        Product::deleteProductData($request);
        return back()->withSuccess('Delete Successful');
    }

    //Product Color
    public function addProductColor(){
        $colors = ProductColor::orderBy('id', 'desc')->get();
        return view('backend.product.add-product-color', compact('colors'));
    }
    public function saveProductColor(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        ProductColor::saveProductColorData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateProductColor(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        ProductColor::updateProductColorData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteProductColor(Request $request){
        ProductColor::deleteProductColorData($request);
        return back()->withSuccess('Delete Successful');
    }

    //Add to Featured
    public function addToFeatured($id){
        $featured = Product::find($id);
        $featured->featured = 1;
        $featured->save();
        return back()->withSuccess('Add to Featured Successful');
    }
    public function removeToFeatured($id){
        $featured = Product::find($id);
        $featured->featured = 0;
        $featured->save();
        return back()->withSuccess('Remove Successful');
    }
    public function featuredProduct(){
        $products = Product::where('featured', 1)->orderBy('id', 'desc')->get();
        $colors = ProductColor::where('status', 1)->orderBy('id', 'desc')->get();
        return view('backend.product.featured-product', compact('products', 'colors'));
    }
}
