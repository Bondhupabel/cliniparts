<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Hash;
use Laravel\Socialite\Facades\Socialite;

class AdminController extends Controller
{
    public function index(){
        $title = 'Admin Login';
        if(Auth::guard('admin')->check()){
            return redirect()->route('admin.dashboard');
        }
        return view('backend.admin.admin_login', compact('title'));
    }
    public function loginCheck(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        if(Auth::guard('admin')->attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])){
            return redirect()->route('admin.dashboard');
        }
        return back()->withErrors('The Combination of Username or Password is Wrong!.');
    }
    public function dashboard(){
        $title = 'Admin Dashboard';
        return view('backend.dashboard', compact('title'));
    }
    public function adminLogout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

    //Register
    public function adminRegister(){
        $title = 'Admin Register';
        return view('backend.admin.register', compact('title'));
    }
    public function saveRegister(Request $request){
        $this->validate($request,[
            'email' => 'required|unique:admins',
            'password' => 'required',
            'phone' => 'unique:admins',
        ]);
        Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role,
            'phone' => $request->phone,
            'address' => $request->address,
        ]);
        return redirect('/admin')->withSuccess('Account Create Successul');
    }

    //Change Password
    public function adminChangePassword(){
        return view('backend.admin.change-password');
    }
    public function saveAdminChangePassword(Request $request){
        $this->validate($request,[
            'current_password' => 'required',
            'password' => 'required',
            'confirm_password' => 'required',
        ]);
        $ok = Admin::find($request->id);
        $password = $request->input('current_password');
        $check = Admin::where('id', Auth::guard('admin')->user()->id)->first();
        if(Hash::check($password, $check->password)) {
            if ($request->password == $request->confirm_password){
                $ok->password = bcrypt($request->password);
                $ok->save();
                return back()->withSuccess('Password Change Successful');
            }
            return back()->withErrors('New Password & Confirm Password Not Match!');
        } else {
            return back()->withErrors('Current Password Not Match');
        }
    }

    //Customer
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }


    public function callback()
    {
//        try {


        $googleUser = Socialite::driver('google')->user();
        $existUser = User::where('email',$googleUser->email)->first();


        if($existUser) {
            Auth::loginUsingId($existUser->id);
        }
        else {
            $user = new User;
            $user->first_name = $googleUser->name;
            $user->email = $googleUser->email;
            $user->google_id = $googleUser->id;
            $user->password = bcrypt(rand(1,10));
            $user->save();
            Auth::loginUsingId($user->id);
        }
        return redirect()->to('/home');
    }
}
