<?php

namespace App\Http\Controllers;

use App\Contact;
use App\StoreLocation;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    //Contact
    public function addContact(){
        $contact = Contact::first();
        return view('backend.contact.add-contact', compact('contact'));
    }
    public function updateContact(Request $request){
        $this->validate($request,[
            'image1' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'image2' => 'mimes:jpeg,bmp,png,jpg,svg,JPEG,JPG,PNG,bmp,gif',
            'title1' => 'required'
        ]);
        Contact::updateContactData($request);
        return back()->withSuccess('Update Successful');
    }

    //Store Location
    public function addStoreLocation(){
        $stores = StoreLocation::orderBy('id', 'desc')->get();
        return view('backend.contact.add-store-location', compact('stores'));
    }
    public function saveStoreLocation(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'des' => 'required',
        ]);
        StoreLocation::saveStoreLocationData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateStoreLocation(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'des' => 'required',
        ]);
        StoreLocation::updateStoreLocationData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteStoreLocation(Request $request){
        StoreLocation::deleteStoreLocationData($request);
        return back()->withSuccess('Delete Successful');
    }
}
