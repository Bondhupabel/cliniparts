<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\AcuteCare;
use App\Assisted;
use App\Blog;
use App\BlogCategory;
use App\BlogTitle;
use App\Category;
use App\Contact;
use App\Education;
use App\HomeHealth;
use App\HomeImage;
use App\HomeSection;
use App\HomeText;
use App\Independent;
use App\Integrated;
use App\LongTerm;
use App\Managed;
use App\News;
use App\NewsCategory;
use App\NewsTitle;
use App\Physician;
use App\PopUp;
use App\Product;
use App\Slider;
use App\StoreLocation;
use App\SubCategory;
use App\SubSubCategory;
use App\Surgery;
use App\TechTip;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        $subscribe = PopUp::first();
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $homeSections = HomeSection::where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        $sliders = Slider::where('status', 1)->orderBy('id', 'desc')->take(5)->get();
        $products = Product::where('status', 1)->orderBy('id', 'desc')->get();
        $featured_products = Product::where('featured', 1)->where('status', 1)->orderBy('id', 'desc')->get();
        return view('frontend.home.home', compact('subscribe', 'homeText', 'homeImage', 'categories', 'subCategories', 'subSubCategories', 'homeSections', 'sliders', 'products', 'featured_products'));
    }
    public function productDetails($id){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $banner = AboutUs::first();
        $product = Product::find($id);
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        if ($product->sub_subcategory_id){
            $subSubCategories = Product::where('sub_subcategory_id', $id)->where('status', 1)->orderBy('id', 'desc')->take(8)->get();
        } else {
            $subCategories = Product::where('subcategory_id', $id)->where('status', 1)->orderBy('id', 'desc')->take(8)->get();
        }
        return view('frontend.product.product-details', compact('homeText', 'homeImage', 'categories', 'subCategories', 'subSubCategories', 'banner', 'product', 'subCategories', 'subSubCategories'));
    }
    public function showIntegrated(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $integrated = Integrated::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-integrated', compact('homeText',  'homeImage', 'integrated', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showAcuteCare(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $acute = AcuteCare::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-acute-care', compact('homeText',  'homeImage', 'acute', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showSurgery(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $surgery = Surgery::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-surgery', compact('homeText',  'homeImage', 'surgery', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showlongTerm(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $long = LongTerm::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-long-term', compact('homeText',  'homeImage', 'long', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showEducation(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $education = Education::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-education', compact('homeText',  'homeImage', 'education', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showHealth(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $health = HomeHealth::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-health', compact('homeText',  'homeImage', 'health', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showPhysician(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $physician = Physician::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-physician', compact('homeText',  'homeImage', 'physician', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showAssisted(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $assisted = Assisted::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-assisted', compact('homeText',  'homeImage', 'assisted', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showManaged(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $managed = Managed::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-managed', compact('homeText',  'homeImage', 'managed', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showIndependent(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $independent = Independent::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.WhoWeServe.show-independent', compact('homeText',  'homeImage', 'independent', 'categories', 'subCategories', 'subSubCategories'));
    }

    //Resources
    public function showTechTips(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $tech = TechTip::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.resources.show-tech-tips', compact('homeText',  'homeImage', 'tech', 'categories', 'subCategories', 'subSubCategories'));
    }
    public function showBlog(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $blogPage = BlogTitle::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $categories = BlogCategory::where('status', 1)->get();
        $blogs = Blog::where('status', 1)->orderBy('id', 'desc')->paginate(5);
        $recentBlogs = Blog::where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        return view('frontend.resources.show-blog', compact('homeText',  'homeImage', 'categories', 'subCategories', 'subSubCategories', 'blogPage', 'categories', 'blogs', 'recentBlogs'));
    }
    public function categoryBlog($id){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $blogPage = BlogTitle::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $categories = BlogCategory::where('status', 1)->get();
        $blogs = Blog::where('category_id', $id)->where('status', 1)->orderBy('id', 'desc')->paginate(5);
        $recentBlogs = Blog::where('category_id', $id)->where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        return view('frontend.resources.show-blog', compact('homeText',  'homeImage', 'categories', 'subCategories', 'subSubCategories', 'blogPage', 'categories', 'blogs', 'recentBlogs'));
    }
    public function blogDetails($id){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $blogPage = BlogTitle::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $categories = BlogCategory::where('status', 1)->get();
        $blog = Blog::find($id);
        $recentBlogs = Blog::where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        return view('frontend.resources.blog-details', compact('homeText',  'homeImage', 'categories', 'subCategories', 'subSubCategories', 'blogPage', 'categories', 'blog', 'recentBlogs'));
    }
    public function showNews(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $newsPage = NewsTitle::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $categories = NewsCategory::where('status', 1)->get();
        $newses = News::where('status', 1)->orderBy('id', 'desc')->paginate(5);
        $recentNewses = News::where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        return view('frontend.resources.show-news', compact('homeText',  'homeImage', 'categories', 'subCategories', 'subSubCategories', 'newsPage', 'categories', 'newses', 'recentNewses'));
    }
    public function categoryNews($id){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $newsPage = NewsTitle::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $categories = NewsCategory::where('status', 1)->get();
        $newses = News::where('category_id', $id)->where('status', 1)->orderBy('id', 'desc')->paginate(5);
        $recentNewses = News::where('category_id', $id)->where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        return view('frontend.resources.show-news', compact('homeText',  'homeImage', 'categories', 'subCategories', 'subSubCategories', 'newsPage', 'categories', 'newses', 'recentNewses'));
    }
    public function newsDetails($id){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $newsPage = NewsTitle::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $categories = NewsCategory::where('status', 1)->get();
        $news = News::find($id);
        $recentNewses = News::where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        return view('frontend.resources.news-details', compact('homeText',  'homeImage', 'categories', 'subCategories', 'subSubCategories', 'newsPage', 'categories', 'news', 'recentNewses'));
    }

    //About
    public function showAbout(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $about = AboutUs::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.about.show-about', compact('homeText',  'homeImage', 'about', 'categories', 'subCategories', 'subSubCategories'));
    }
    //Contact
    public function showContact(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $contact = Contact::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        $stores = StoreLocation::where('status', 1)->get();
        return view('frontend.contact.show-contact', compact('homeText',  'homeImage', 'contact', 'stores', 'categories', 'subCategories', 'subSubCategories'));
    }

    //Checkout
    public function checkout(){
        $homeText = HomeText::first();
        $homeImage = HomeImage::first();
        $banner = AboutUs::first();
        $categories = Category::where('status', 1)->get();
        $subCategories = SubCategory::where('status', 1)->get();
        $subSubCategories = SubSubCategory::where('status', 1)->get();
        return view('frontend.checkout.checkout', compact('homeText', 'homeImage', 'banner', 'categories', 'subCategories', 'subSubCategories'));
    }

    //Add To Cart
    public function addToCart($id){
        $product = Product::find($id);
        return response()->json(['success'=>$product]);
    }

    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }
    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }
}
