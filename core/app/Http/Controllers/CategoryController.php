<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function addCategory(){
        $categories = Category::orderBy('id', 'desc')->get();
        return view('backend.category.add-category', compact('categories'));
    }
    public function saveCategory(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        Category::addCategoryData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateCategory(Request $request){
        Category::updateCategoryData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteCategory(Request $request){
        Category::deleteCategoryData($request);
        return back()->withSuccess('Delete Successful');
    }

    //Sub Category
    public function addSubCategory(){
        $categories = Category::where('status', 1)->orderBy('id', 'desc')->get();
        $subCategories = SubCategory::orderBy('id', 'desc')->get();
        return view('backend.category.add-subCategory',compact('categories','subCategories'));
    }
    public function saveSubCategory(Request $request){
        $this->validate($request,[
            'category_id' => 'required',
            'name' => 'required',
        ]);
        SubCategory::addSubCategoryData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateSubCategory(Request $request){
        $this->validate($request,[
            'category_id' => 'required',
            'name' => 'required',
        ]);
        SubCategory::updateSubCategoryData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteSubCategory(Request $request){
        SubCategory::deleteSubCategoryData($request);
        return back()->withSuccess('Delete Successful');
    }

    //ajax
    public function getCategory(Request $request){
        $id = $request->get('id');
        $subCategories = SubCategory::where('category_id', $id)->where('status', 1)->get();

        echo '<option selected disabled value="" >---SELECT SUB CATEGORY---</option>';
        foreach ($subCategories as $subCategory){
            echo '<option value="'.$subCategory->id.'">'.$subCategory->name.'</option>';
        }
    }
    public function getSubCategory(Request $request){
        $id = $request->get('id');
        $subSubCategories = SubSubCategory::where('subcategory_id', $id)->where('status', 1)->get();

        echo '<option selected disabled value="" >---SELECT SUB SUB CATEGORY---</option>';
        foreach ($subSubCategories as $subSubCategory){
            echo '<option value="'.$subSubCategory->id.'">'.$subSubCategory->name.'</option>';
        }
    }

    //Sub SubCategory
    public function addSubSubCategory(){
        $categories = Category::where('status', 1)->orderBy('id', 'desc')->get();
        $subCategories = SubCategory::where('status', 1)->orderBy('id', 'desc')->get();
        $subSubCategories = SubSubCategory::orderBy('id', 'desc')->get();
        return view('backend.category.add-subSubCategory', compact('categories','subCategories','subSubCategories'));
    }
    public function saveSubSubCategory(Request $request){
        $this->validate($request,[
            'subcategory_id' => 'required',
            'name' => 'required',
        ]);
        SubSubCategory::addSubSubCategoryData($request);
        return back()->withSuccess('Save Successful');
    }
    public function updateSubSubCategory(Request $request){
        $this->validate($request,[
            'subcategory_id' => 'required',
            'name' => 'required'
        ]);
        SubSubCategory::updateSubSubCategoryData($request);
        return back()->withSuccess('Update Successful');
    }
    public function deleteSubSubCategory(Request $request){
        SubSubCategory::deleteSubSubCategoryData($request);
        return back()->withSuccess('Delete Successful');
    }
}
