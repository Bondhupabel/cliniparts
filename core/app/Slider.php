<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Slider extends Model
{
    protected $guarded = ['id'];

    public static function addSliderData($request){
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(400, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
        }
        Slider::create([
            'text' => $request->text,
            'button' => $request->button,
            'link' => $request->link,
            'image' => $request->hasFile('image') ? $imageName : null,
            'status' => $request->status,
        ]);
    }
    public static function updateSliderData($request){
        $slider = Slider::find($request->id);
        if ($request->file('image')){
            @unlink('assets/backend/images/HomePage/'.$slider->image);
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/HomePage/'.$imageName;
            Image::make($image)->resize(400, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $slider->image = $imageName;
        }
        $slider->text = $request->text;
        $slider->button = $request->button;
        $slider->link = $request->link;
        $slider->status = $request->status;
        $slider->save();
    }
    public static function deleteSliderData($request){
        $slider = Slider::find($request->id);
        @unlink('assets/backend/images/HomePage/'.$slider->image);
        $slider->delete();
    }
}
