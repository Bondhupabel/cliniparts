<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{
    protected $guarded = [];

    public static function saveProductColorData($request){
        ProductColor::create([
            'name' => $request->name,
            'status' => $request->status,
        ]);
    }
    public static function updateProductColorData($request){
        $color = ProductColor::find($request->id);
        $color->name = $request->name;
        $color->status = $request->status;
        $color->save();
    }
    public static function deleteProductColorData($request){
        $color = ProductColor::find($request->id);
        $color->delete();
    }
}
