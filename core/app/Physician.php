<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class physician extends Model
{
    public static function addPhysicianData($request){
        $physician = Physician::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$physician->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $physician->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$physician->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $physician->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$physician->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $physician->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$physician->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $physician->image4 = $imageName;
        }
        $physician->title1 = $request->title1;
        $physician->title2 = $request->title2;
        $physician->m_title = $request->m_title;
        $physician->m_des = $request->m_des;
        $physician->s_title1 = $request->s_title1;
        $physician->s_des1 = $request->s_des1;
        $physician->s_title2 = $request->s_title2;
        $physician->s_des2 = $request->s_des2;
        $physician->s_title3 = $request->s_title3;
        $physician->s_des3 = $request->s_des3;
        $physician->s_title4 = $request->s_title4;
        $physician->s_des4 = $request->s_des4;
        $physician->s_title5 = $request->s_title5;
        $physician->s_des5 = $request->s_des5;
        $physician->save();
    }
}
