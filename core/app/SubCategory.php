<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $guarded = [];

    public function categoryName(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public static function addSubCategoryData($request){
        SubCategory::create([
            'category_id' => $request->category_id,
            'name' => $request->name,
            'status' => $request->status,
        ]);
    }
    public static function updateSubCategoryData($request){
        $subCategory = SubCategory::find($request->id);
        $subCategory->category_id = $request->category_id;
        $subCategory->name = $request->name;
        $subCategory->status = $request->status;
        $subCategory->save();
    }
    public static function deleteSubCategoryData($request){
        $subCategory = SubCategory::find($request->id);
        $subCategory->delete();
    }
}
