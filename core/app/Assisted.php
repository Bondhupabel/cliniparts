<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Assisted extends Model
{
    public static function addAssistedData($request){
        $assisted = Assisted::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$assisted->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $assisted->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$assisted->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $assisted->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$assisted->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $assisted->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$assisted->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $assisted->image4 = $imageName;
        }
        $assisted->title1 = $request->title1;
        $assisted->title2 = $request->title2;
        $assisted->m_title = $request->m_title;
        $assisted->m_des = $request->m_des;
        $assisted->s_title1 = $request->s_title1;
        $assisted->s_des1 = $request->s_des1;
        $assisted->s_title2 = $request->s_title2;
        $assisted->s_des2 = $request->s_des2;
        $assisted->s_title3 = $request->s_title3;
        $assisted->s_des3 = $request->s_des3;
        $assisted->s_title4 = $request->s_title4;
        $assisted->s_des4 = $request->s_des4;
        $assisted->s_title5 = $request->s_title5;
        $assisted->s_des5 = $request->s_des5;
        $assisted->save();
    }
}
