<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Image;

class Blog extends Model
{
    protected $guarded = [];

    public function getCategory(){
        return $this->belongsTo(BlogCategory::class, 'category_id', 'id');
    }
    public function authorName(){
        return $this->belongsTo(Admin::class, 'author_id', 'id');
    }

    public static function addBlogData($request){
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/Blog/'.$imageName;
            Image::make($image)->resize(500, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
        }
        Blog::create([
            'category_id' => $request->category_id,
            'title' => $request->title,
            'author_id' => Auth::guard('admin')->user()->id,
            'image' => $request->hasFile('image') ? $imageName : null,
            'des' => $request->des,
            'status' => $request->status,
        ]);
    }
    public static function updateBlogData($request){
        $blog = Blog::find($request->id);
        if ($request->file('image')){
            @unlink('assets/backend/images/Blog/'.$blog->image);
            $image = $request->file('image');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/Blog/'.$imageName;
            Image::make($image)->resize(500, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $blog->image = $imageName;
        }
        $blog->category_id = $request->category_id;
        $blog->title = $request->title;
        $blog->des = $request->des;
        $blog->status = $request->status;
        $blog->save();
    }
    public static function deleteBlogData($request){
        $blog = Blog::find($request->id);
        @unlink('assets/backend/images/Blog/'. $blog->image);
        $blog->delete();
    }
}
