<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Independent extends Model
{
    public static function addIndependentData($request){
        $independent = Independent::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$independent->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $independent->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$independent->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $independent->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$independent->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $independent->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$independent->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $independent->image4 = $imageName;
        }
        $independent->title1 = $request->title1;
        $independent->title2 = $request->title2;
        $independent->m_title = $request->m_title;
        $independent->m_des = $request->m_des;
        $independent->s_title1 = $request->s_title1;
        $independent->s_des1 = $request->s_des1;
        $independent->s_title2 = $request->s_title2;
        $independent->s_des2 = $request->s_des2;
        $independent->s_title3 = $request->s_title3;
        $independent->s_des3 = $request->s_des3;
        $independent->s_title4 = $request->s_title4;
        $independent->s_des4 = $request->s_des4;
        $independent->s_title5 = $request->s_title5;
        $independent->s_des5 = $request->s_des5;
        $independent->save();
    }
}
