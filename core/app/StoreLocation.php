<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreLocation extends Model
{
    protected $guarded = ['id'];

    public static function saveStoreLocationData($request){
        StoreLocation::create([
            'name' => $request->name,
            'des' => $request->des,
            'status' => $request->status,
        ]);
    }
    public static function updateStoreLocationData($request){
        $store = StoreLocation::find($request->id);
        $store->name = $request->name;
        $store->des = $request->des;
        $store->status = $request->status;
        $store->save();
    }
    public static function deleteStoreLocationData($request){
        $store = StoreLocation::find($request->id);
        $store->delete();
    }
}
