<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class TechTip extends Model
{
    public static function addTechTipsData($request){
        $tech = TechTip::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/TechTips/'.$tech->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/TechTips/'.$imageName;
            Image::make($image)->resize(400, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $tech->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/TechTips/'.$tech->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/TechTips/'.$imageName;
            Image::make($image)->resize(400, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $tech->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/TechTips/'.$tech->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/TechTips/'.$imageName;
            Image::make($image)->resize(500, 400, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $tech->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/TechTips/'.$tech->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/TechTips/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $tech->image4 = $imageName;
        }
        $tech->title1 = $request->title1;
        $tech->title2 = $request->title2;
        $tech->des1 = $request->des1;
        $tech->des2 = $request->des2;
        $tech->save();
    }
}
