<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Image;

class Product extends Model
{
    protected $guarded = [];

    public function subSubCategoryName(){
        return $this->belongsTo(SubSubCategory::class, 'sub_subcategory_id', 'id');
    }
    public function getUserCreate(){
        return $this->belongsTo(Admin::class, 'created_by', 'id');
    }
    public function getUserUpdate(){
        return $this->belongsTo(Admin::class, 'updated_by', 'id');
    }

    public static function addProductData($request){
        if ($request->hasFile('image1')){
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(300,300, function ($constraint){
                $constraint->aspectRatio();
            })->save($location);
        }
        if ($request->hasFile('image2')){
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(200,200, function ($constraint){
                $constraint->aspectRatio();
            })->save($location);
        }
        if ($request->hasFile('image3')){
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(200,200, function ($constraint){
                $constraint->aspectRatio();
            })->save($location);
        }
        if ($request->hasFile('image4')){
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(200,200, function ($constraint){
                $constraint->aspectRatio();
            })->save($location);
        }
        Product::create([
            'subcategory_id' => $request->subcategory_id,
            'sub_subcategory_id' => $request->sub_subcategory_id,
            'color_id' => json_encode($request->color_id),
            'name' => $request->name,
            'image1' => $request->hasFile('image1') ? $imageName : null,
            'image2' => $request->hasFile('image2') ? $imageName : null,
            'image3' => $request->hasFile('image3') ? $imageName : null,
            'image4' => $request->hasFile('image4') ? $imageName : null,
            'top_title' => $request->top_title,
            'price' => $request->price,
            'old_price' => $request->old_price,
            'product_code' => $request->product_code,
            'amount' => $request->amount,
            's_des' => $request->s_des,
            'des' => $request->des,
            'specification' => $request->specification,
            'created_by' => Auth::guard('admin')->user()->id,
        ]);
    }
    public static function updateProductData($request){
        $product = Product::find($request->id);
        if($request->hasFile('image1')){
            @unlink('assets/backend/images/Product/'.$product->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            $product->image1 = $imageName;
        }
        if($request->hasFile('image2')){
            @unlink('assets/backend/images/Product/'.$product->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            $product->image2 = $imageName;
        }
        if($request->hasFile('image3')){
            @unlink('assets/backend/images/Product/'.$product->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            $product->image3 = $imageName;
        }
        if($request->hasFile('image4')){
            @unlink('assets/backend/images/Product/'.$product->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $location = 'assets/backend/images/Product/'.$imageName;
            Image::make($image)->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            $product->image4 = $imageName;
        }
        $product->subcategory_id = $request->subcategory_id;
        $product->sub_subcategory_id = $request->sub_subcategory_id;
        $product->color_id = json_encode($request->color_id);
        $product->name = $request->name;
        $product->top_title = $request->top_title;
        $product->price = $request->price;
        $product->old_price = $request->old_price;
        $product->product_code = $request->product_code;
        $product->amount = $request->amount;
        $product->s_des = $request->s_des;
        $product->des = $request->des;
        $product->specification = $request->specification;
        $product->status = $request->status;
        $product->updated_by = Auth::guard('admin')->user()->id;
        $product->save();
    }
    public static function deleteProductData($request){
        $product = Product::find($request->id);
        @unlink('assets/backend/images/Product/'.$product->image1);
        @unlink('assets/backend/images/Product/'.$product->image2);
        @unlink('assets/backend/images/Product/'.$product->image3);
        @unlink('assets/backend/images/Product/'.$product->image4);
        $product->delete();
    }
}
