<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Surgery extends Model
{
    public static function addSurgeryData($request){
        $surgery = Surgery::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$surgery->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $surgery->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$surgery->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $surgery->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$surgery->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $surgery->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$surgery->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $surgery->image4 = $imageName;
        }
        $surgery->title1 = $request->title1;
        $surgery->title2 = $request->title2;
        $surgery->m_title = $request->m_title;
        $surgery->m_des = $request->m_des;
        $surgery->s_title1 = $request->s_title1;
        $surgery->s_des1 = $request->s_des1;
        $surgery->s_title2 = $request->s_title2;
        $surgery->s_des2 = $request->s_des2;
        $surgery->s_title3 = $request->s_title3;
        $surgery->s_des3 = $request->s_des3;
        $surgery->s_title4 = $request->s_title4;
        $surgery->s_des4 = $request->s_des4;
        $surgery->s_title5 = $request->s_title5;
        $surgery->s_des5 = $request->s_des5;
        $surgery->save();
    }
}
