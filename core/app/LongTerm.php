<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class LongTerm extends Model
{
    public static function addLongTermData($request){
        $long = LongTerm::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$long->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $long->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$long->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $long->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$long->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $long->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$long->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $long->image4 = $imageName;
        }
        $long->title1 = $request->title1;
        $long->title2 = $request->title2;
        $long->m_title = $request->m_title;
        $long->m_des = $request->m_des;
        $long->s_title1 = $request->s_title1;
        $long->s_des1 = $request->s_des1;
        $long->s_title2 = $request->s_title2;
        $long->s_des2 = $request->s_des2;
        $long->s_title3 = $request->s_title3;
        $long->s_des3 = $request->s_des3;
        $long->s_title4 = $request->s_title4;
        $long->s_des4 = $request->s_des4;
        $long->s_title5 = $request->s_title5;
        $long->s_des5 = $request->s_des5;
        $long->save();
    }
}
