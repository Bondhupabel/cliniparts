<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Education extends Model
{
    public static function addEducationData($request){
        $education = Education::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$education->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $education->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$education->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $education->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$education->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $education->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$education->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $education->image4 = $imageName;
        }
        $education->title1 = $request->title1;
        $education->title2 = $request->title2;
        $education->m_title = $request->m_title;
        $education->m_des = $request->m_des;
        $education->s_title1 = $request->s_title1;
        $education->s_des1 = $request->s_des1;
        $education->s_title2 = $request->s_title2;
        $education->s_des2 = $request->s_des2;
        $education->s_title3 = $request->s_title3;
        $education->s_des3 = $request->s_des3;
        $education->s_title4 = $request->s_title4;
        $education->s_des4 = $request->s_des4;
        $education->s_title5 = $request->s_title5;
        $education->s_des5 = $request->s_des5;
        $education->save();
    }
}
