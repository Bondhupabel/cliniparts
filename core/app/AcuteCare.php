<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class AcuteCare extends Model
{
    public static function addAcuteCareData($request){
        $acute = AcuteCare::first();
        if ($request->file('image1')){
            @unlink('assets/backend/images/WhoWeServe/'.$acute->image1);
            $image = $request->file('image1');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $acute->image1 = $imageName;
        }
        if ($request->file('image2')){
            @unlink('assets/backend/images/WhoWeServe/'.$acute->image2);
            $image = $request->file('image2');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $acute->image2 = $imageName;
        }
        if ($request->file('image3')){
            @unlink('assets/backend/images/WhoWeServe/'.$acute->image3);
            $image = $request->file('image3');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $acute->image3 = $imageName;
        }
        if ($request->file('image4')){
            @unlink('assets/backend/images/WhoWeServe/'.$acute->image4);
            $image = $request->file('image4');
            $imageName = $image->hashName();
            $directory = 'assets/backend/images/WhoWeServe/'.$imageName;
            Image::make($image)->resize(300, 300, function($constraint) { $constraint->aspectRatio();
            })->save($directory, $imageName);
            $acute->image4 = $imageName;
        }
        $acute->title1 = $request->title1;
        $acute->title2 = $request->title2;
        $acute->m_title = $request->m_title;
        $acute->m_des = $request->m_des;
        $acute->s_title1 = $request->s_title1;
        $acute->s_des1 = $request->s_des1;
        $acute->s_title2 = $request->s_title2;
        $acute->s_des2 = $request->s_des2;
        $acute->s_title3 = $request->s_title3;
        $acute->s_des3 = $request->s_des3;
        $acute->s_title4 = $request->s_title4;
        $acute->s_des4 = $request->s_des4;
        $acute->s_title5 = $request->s_title5;
        $acute->s_des5 = $request->s_des5;
        $acute->save();
    }
}
