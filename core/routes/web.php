<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Frontend
Route::get('/redirect', 'AdminController@redirect');
Route::get('/callback', 'AdminController@callback');

Route::get('/', 'IndexController@index')->name('/');
Route::get('/product/details/{id}', 'IndexController@productDetails')->name('product.details');

Route::get('/add-to-cart/{id}', 'IndexController@addToCart')->name('add.to.cart');
Route::patch('update-cart', 'IndexController@update');
Route::delete('/remove-from-cart', 'IndexController@remove');

Route::get('/checkout', 'IndexController@checkout')->name('checkout');
//Who We Serve
Route::get('/integrated/delivery/network', 'IndexController@showIntegrated')->name('show.integrated');
Route::get('/acute/care', 'IndexController@showAcuteCare')->name('show.acute.care');
Route::get('/surgery/centers', 'IndexController@showSurgery')->name('show.surgery');
Route::get('/long-term/care', 'IndexController@showlongTerm')->name('show.long.term');
Route::get('/education/research', 'IndexController@showEducation')->name('show.education');
Route::get('/home/health/hospice', 'IndexController@showHealth')->name('show.health');
Route::get('/physician/officers', 'IndexController@showPhysician')->name('show.physician');
Route::get('/assisted/living', 'IndexController@showAssisted')->name('show.assisted');
Route::get('/managed/care', 'IndexController@showManaged')->name('show.managed');
Route::get('/independent/service/organizations', 'IndexController@showIndependent')->name('show.independent');

Route::get('/tech/tips', 'IndexController@showTechTips')->name('show.tech.tips');
Route::get('/blog', 'IndexController@showBlog')->name('show.blog');
Route::get('/category/blog/{id}', 'IndexController@categoryBlog')->name('category.blog');
Route::get('/blog/details/{id}', 'IndexController@blogDetails')->name('blog.details');

Route::get('/news', 'IndexController@showNews')->name('show.news');
Route::get('/category/news/{id}', 'IndexController@categoryNews')->name('category.news');
Route::get('/news/details/{id}', 'IndexController@newsDetails')->name('news.details');

Route::get('/about', 'IndexController@showAbout')->name('show.about');
Route::get('/contact', 'IndexController@showContact')->name('show.contact');

Route::post('/send/subscribe', 'SubscribeController@sendSubscribe')->name('send.subscribe');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Admin Panel
Route::group(['prefix' => 'admin'], function (){
    Route::get('/', 'AdminController@index')->name('admin.login');
    Route::post('/login', 'AdminController@loginCheck')->name('admin.loginCheck');

    Route::get('/register', 'AdminController@adminRegister')->name('admin.register');
    Route::post('/save/register', 'AdminController@saveRegister')->name('save.register');

    Route::middleware('auth:admin')->group(function (){
        Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
        Route::get('/logout', 'AdminController@adminLogout')->name('admin.logout');

        //Change Password
        Route::get('/change/password', 'AdminController@adminChangePassword')->name('admin.change.password');
        Route::post('/save/change/password', 'AdminController@saveAdminChangePassword')->name('save.admin.change.password');

        //Slider
        Route::get('/add/slider', 'HomePageController@addSlider')->name('add.slider');
        Route::post('/save/slider', 'HomePageController@saveSlider')->name('save.slider');
        Route::post('/update/slider', 'HomePageController@updateSlider')->name('update.slider');
        Route::post('/delete/slider', 'HomePageController@deleteSlider')->name('delete.slider');
        //HomePage Text
        Route::get('/add/home/text', 'HomePageController@addHomeText')->name('add.home.text');
        Route::post('/update/home/text', 'HomePageController@updateHomeText')->name('update.home.text');
        //HomePage Image
        Route::get('/add/home/image', 'HomePageController@addHomeImage')->name('add.home.image');
        Route::post('/update/home/image', 'HomePageController@updateHomeImage')->name('update.home.image');
        //Home Section
        Route::get('/add/home/section', 'HomePageController@addHomeSection')->name('add.home.section');
        Route::post('/save/home/section', 'HomePageController@saveHomeSection')->name('save.home.section');
        Route::post('/update/home/section', 'HomePageController@updateHomeSection')->name('update.home.section');
        Route::post('/delete/home/section', 'HomePageController@deleteHomeSection')->name('delete.home.section');
        //Subscribe PopUp
        Route::get('/add/home/popup', 'HomePageController@addHomePopUp')->name('add.home.popup');
        Route::post('/update/home/popup', 'HomePageController@updateHomePopUp')->name('update.home.popup');

        //Integrated Delivery Network
        Route::get('/add/integrated', 'WhoWeServeController@addIntegrated')->name('add.integrated');
        Route::post('/update/integrated', 'WhoWeServeController@updateIntegrated')->name('update.integrated');

        Route::get('/add/acute', 'WhoWeServeController@addAcute')->name('add.acute');
        Route::post('/update/acute', 'WhoWeServeController@updateAcute')->name('update.acute');

        Route::get('/add/surgery', 'WhoWeServeController@addSurgery')->name('add.surgery');
        Route::post('/update/surgery', 'WhoWeServeController@updateSurgery')->name('update.surgery');

        Route::get('/add/long/term', 'WhoWeServeController@addLongTerm')->name('add.long.term');
        Route::post('/update/long/term', 'WhoWeServeController@updateLongTerm')->name('update.long.term');

        Route::get('/add/education', 'WhoWeServeController@addEducation')->name('add.education');
        Route::post('/update/education', 'WhoWeServeController@updateEducation')->name('update.education');

        Route::get('/add/home/health', 'WhoWeServeController@addHomeHealth')->name('add.home.health');
        Route::post('/update/home/health', 'WhoWeServeController@updateHomeHealth')->name('update.home.health');

        Route::get('/add/physician', 'WhoWeServeController@addPhysician')->name('add.physician');
        Route::post('/update/physician', 'WhoWeServeController@updatePhysician')->name('update.physician');

        Route::get('/add/assisted', 'WhoWeServeController@addAssisted')->name('add.assisted');
        Route::post('/update/assisted', 'WhoWeServeController@updateAssisted')->name('update.assisted');

        Route::get('/add/managed', 'WhoWeServeController@addManaged')->name('add.managed');
        Route::post('/update/managed', 'WhoWeServeController@updateManaged')->name('update.managed');

        Route::get('/add/independent', 'WhoWeServeController@addIndependent')->name('add.independent');
        Route::post('/update/independent', 'WhoWeServeController@updateIndependent')->name('update.independent');

        /*Resources*/
        //Tech Tips
        Route::get('/add/tech/tips', 'ResourcesController@addTechTips')->name('add.tech.tips');
        Route::post('/update/tech/tips', 'ResourcesController@updateTechTips')->name('update.tech.tips');
        //blog
        Route::get('/add/blog/title', 'ResourcesController@addBlogTitle')->name('add.blog.title');
        Route::post('/update/blog/title', 'ResourcesController@updateBlogTitle')->name('update.blog.title');

        Route::get('/add/blog/category', 'ResourcesController@addBlogCategory')->name('add.blog.category');
        Route::post('/save/blog/category', 'ResourcesController@saveBlogCategory')->name('save.blog.category');
        Route::post('/update/blog/category', 'ResourcesController@updateBlogCategory')->name('update.blog.category');
        Route::post('/delete/blog/category', 'ResourcesController@deleteBlogCategory')->name('delete.blog.category');

        Route::get('/add/blog', 'ResourcesController@addBlog')->name('add.blog');
        Route::post('/save/blog', 'ResourcesController@saveBlog')->name('save.blog');
        Route::post('/update/blog', 'ResourcesController@updateBlog')->name('update.blog');
        Route::post('/delete/blog', 'ResourcesController@deleteBlog')->name('delete.blog');

        //News Room
        Route::get('/add/news/title', 'ResourcesController@addNewsTitle')->name('add.news.title');
        Route::post('/update/news/title', 'ResourcesController@updateNewsTitle')->name('update.news.title');

        Route::get('/add/news/category', 'ResourcesController@addNewsCategory')->name('add.news.category');
        Route::post('/save/news/category', 'ResourcesController@saveNewsCategory')->name('save.news.category');
        Route::post('/update/news/category', 'ResourcesController@updateNewsCategory')->name('update.news.category');
        Route::post('/delete/news/category', 'ResourcesController@deleteNewsCategory')->name('delete.news.category');

        Route::get('/add/news', 'ResourcesController@addNews')->name('add.news');
        Route::post('/save/news', 'ResourcesController@saveNews')->name('save.news');
        Route::post('/update/news', 'ResourcesController@updateNews')->name('update.news');
        Route::post('/delete/news', 'ResourcesController@deleteNews')->name('delete.news');

        //About Us
        Route::get('/add/about/us', 'AboutUsController@addAboutUs')->name('add.about.us');
        Route::post('/update/about/us', 'AboutUsController@updateAboutUs')->name('update.about.us');
        //Contact
        Route::get('/add/contact', 'ContactController@addContact')->name('add.contact');
        Route::post('/update/contact', 'ContactController@updateContact')->name('update.contact');

        Route::get('/add/store/location', 'ContactController@addStoreLocation')->name('add.store.location');
        Route::post('/save/store/location', 'ContactController@saveStoreLocation')->name('save.store.location');
        Route::post('/update/store/location', 'ContactController@updateStoreLocation')->name('update.store.location');
        Route::post('/delete/store/location', 'ContactController@deleteStoreLocation')->name('delete.store.location');

        //Subscribe Email
        Route::get('/show/subscribe/email', 'SubscribeController@showSubscribeEmail')->name('show.subscribe.email');
        Route::post('/delete/subscribe/email', 'SubscribeController@deleteSubscribeEmail')->name('delete.subscribe.email');

        //Category
        Route::get('/add/category', 'CategoryController@addCategory')->name('add.category');
        Route::post('/save/category', 'CategoryController@saveCategory')->name('save.category');
        Route::post('/update/category', 'CategoryController@updateCategory')->name('update.category');
        Route::post('/delete/category', 'CategoryController@deleteCategory')->name('delete.category');
        //SubCategory
        Route::get('/add/subCategory', 'CategoryController@addSubCategory')->name('add.subCategory');
        Route::post('/save/subCategory', 'CategoryController@saveSubCategory')->name('save.subCategory');
        Route::post('/update/subCategory', 'CategoryController@updateSubCategory')->name('update.subCategory');
        Route::post('/delete/subCategory', 'CategoryController@deleteSubCategory')->name('delete.subCategory');
        //SubSubCategory
        Route::get('/get-category', 'CategoryController@getCategory'); //ajax request
        Route::get('/add/subSubCategory', 'CategoryController@addSubSubCategory')->name('add.subSubCategory');
        Route::post('/save/subSubCategory', 'CategoryController@saveSubSubCategory')->name('save.subSubCategory');
        Route::post('/update/subSubCategory', 'CategoryController@updateSubSubCategory')->name('update.subSubCategory');
        Route::post('/delete/subSubCategory', 'CategoryController@deletesubSubCategory')->name('delete.subSubCategory');

        //Product
        Route::get('/get-subcategory', 'ProductController@getSubCategory'); //Ajax Request
        Route::get('/add/product', 'ProductController@addProduct')->name('add.product');
        Route::post('/save/product', 'ProductController@saveProduct')->name('save.product');
        Route::get('/manage/product', 'ProductController@manageProduct')->name('manage.product');
        Route::get('/view/product/{id}', 'ProductController@viewProduct')->name('view.product');
        Route::get('/edit/product/{id}', 'ProductController@editProduct')->name('edit.product');
        Route::post('/update/product', 'ProductController@updateProduct')->name('update.product');
        Route::post('/delete/product', 'ProductController@deleteProduct')->name('delete.product');
        //Product Color
        Route::get('/add/product/color', 'ProductController@addProductColor')->name('add.product.color');
        Route::post('/save/product/color', 'ProductController@saveProductColor')->name('save.product.color');
        Route::post('/update/product/color', 'ProductController@updateProductColor')->name('update.product.color');
        Route::post('/delete/product/color', 'ProductController@deleteProductColor')->name('delete.product.color');
        //Add to Featured
        Route::get('/add/to/featured/{id}', 'ProductController@addToFeatured')->name('add.to.featured');
        Route::get('/remove/to/featured/{id}', 'ProductController@removeToFeatured')->name('remove.to.featured');
        Route::get('/featured/product', 'ProductController@featuredProduct')->name('featured.product');
    });
});
